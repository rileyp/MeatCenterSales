import { MemoryRouter as Router, Routes, Route } from 'react-router-dom';
import Invoice from "../pages/Invoice";
import Inventory from "../pages/Inventory";
import Financial from "../pages/Financial";
import Footer from '../components/Footer';
import Settings from "../pages/Settings";
import Transaction_Success from '../pages/Transaction_Success';


import NavBar from "../components/NavBar";

import './App.css';
import CarcassInvoicePage from '../pages/CarcassInvoicesPage';


export default function App() {
  return (
    <Router>
      <NavBar></NavBar>
      <div style={{paddingTop: '0px', paddingBottom: '0px', minHeight: '72vh'}}>
      <Routes>
        <Route path="/" element={<Invoice />} />
        <Route path="/inventory" element={<Inventory />} />
        <Route path="/financial" element={<Financial/>} />
        <Route path="/settings" element={<Settings/>} />
        <Route path="/transaction_success" element={<Transaction_Success/>} />
        <Route path="/carcass_invoices" element={<CarcassInvoicePage />} />
      </Routes>
      </div>
      <Footer></Footer>
    </Router>
  );
}
