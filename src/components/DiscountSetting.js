import React, { useState, useEffect } from 'react';
import { Button, Typography, TextField, Box, Alert } from '@mui/material';

function DiscountSetting() {
  const [discountRate, setDiscountRate] = useState('');
  const [showAlert, setShowAlert] = useState(false);
  const [currDiscountRate, setCurrDiscountRate] = useState('');

  useEffect(() => {
    // Initialize discountRate state when the component mounts
    setCurrDiscountRate(localStorage.getItem('discountRate') || '');
  }, [discountRate]);

  // local storage function to save discountrate
  const saveDiscountRate = () => {
    localStorage.setItem('discountRate', discountRate);
    setShowAlert(true);
    //setCurrDiscountRate(discountRate);
    setDiscountRate(''); // Reset discountRate after saving
    setTimeout(() => {
      setShowAlert(false);
    }, 2000);
  };

  // function to update discount rate
  const handleDiscountChange = (e) => {
    const input = e.target.value;
    // regular expression to set only between 0 to 100
    if (/^\d*$/.test(input) && input >= 0 && input <= 100) {
      setDiscountRate(input);
    }
  };

  const isSaveDisabled =
    !discountRate || !(discountRate >= 0 && discountRate <= 100);

  return (
    <div
      style={{
        border: '1px solid #ccc',
        borderRadius: '10px',
        padding: '20px',
        marginTop: 40,
        width: '40%',
        margin: '40px auto 0',
      }}
    >
      <Typography variant="h6">Discount ({currDiscountRate}%)</Typography>
      <Box sx={{ display: 'flex', alignItems: 'center', marginTop: '10px' }}>
        <TextField
          label="Enter Discount Rate"
          variant="outlined"
          value={discountRate}
          onChange={handleDiscountChange}
          type="text"
          inputMode="numeric"
          pattern="[0-9]*"
          InputProps={{ inputProps: { min: 0, max: 100 } }}
          sx={{ width: '200px' }}
        />
        <Button
          variant="contained"
          onClick={saveDiscountRate}
          style={{ marginLeft: '10px', height: '50px' }}
          disabled={isSaveDisabled}
        >
          Save
        </Button>
      </Box>
      {/* Alert to indicate successful update */}
      {showAlert && (
        <Alert severity="success" onClose={() => setShowAlert(false)}>
          Discount rate successfully updated!
        </Alert>
      )}
    </div>
  );
}

export default DiscountSetting;
