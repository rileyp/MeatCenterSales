import * as React from 'react';
import PropTypes from 'prop-types';
import InventorySection from '../components/InventorySection';
import Typography from '@mui/material/Typography';

//need to make a list of Inventory Category, then that should be the only component in Inventory.js page

function InventoryCategory({ category }) {
  return (
    <div
      style={{
        border: '1px solid #ccc',
        padding: '15px',
        borderRadius: '10px',
      }}
    >
      {/* <h3>InventoryCategory category: {JSON.stringify(category)}</h3> */}
      <Typography
        variant="h4"
        component="div"
        gutterBottom
        // style={{ textAlign: 'center' }}
      >
        {category.category.categoryId}: {category.category.name}
      </Typography>
      {category.subcategories.map((subcategory, idx) => (
        <div key={idx}>
          <InventorySection
            subCategory={subcategory}
            rows={subcategory.products}
            //subCategory={subcategory}
          />
        </div>
      ))}
    </div>
  );
}

export default InventoryCategory;
