import React from 'react';
import ReceiptForm from './ReceiptForm';
import { useLocation } from 'react-router-dom';
import PDF from './CreateInvoice';
import { useState, useEffect } from 'react';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  TextField,
  Box,
} from '@mui/material';
import { RadioGroup, FormLabel, Radio, Typography } from '@mui/material';

function PrintInvoice(props) {
  const location = useLocation();

  const [receiptEmail, setReceiptEmail] = useState('');
  const [receiptSalesRep, setReceiptSalesRep] = useState('');
  const [showPrintModal, setShowPrintModal] = useState(false);
  const [pdfDoc, setPDFDoc] = useState('');
  const [pdfSrc, setPDFSrc] = useState('');

  const invoiceData = props.invoice;
  //   const [numPages, setNumPages] = useState(null);
  //   const [pageNumber, setPageNumber] = useState(1);
  //   const [showPreview, setShowPreview] = useState(false);
  //   const [pdfBlob, setPdfBlob] = useState(null);

  useEffect(() => {
    const storedEmail = localStorage.getItem('receiptEmail');
    const storedSalesRep = localStorage.getItem('receiptSalesRep');

    if (storedEmail) setReceiptEmail(storedEmail);
    if (storedSalesRep) setReceiptSalesRep(storedSalesRep);
    //printInvoice(invoiceData);
  }, []);

  useEffect(() => {
    printInvoice(invoiceData);
  }, [receiptEmail, receiptSalesRep]);

  const handleDownloadPDF = () => {
    downloadInvoice();
  };

  const handlePrintPDF = () => {
    setShowPrintModal(true);
    printInvoice(invoiceData);
  };

  const handleCompletedPrintPDF = () => {
    setShowPrintModal(false);
  };

  const downloadInvoice = (invoice) => {
    pdfDoc.doc.save(`${pdfDoc.invoiceNumber}.pdf`);
  };

  const printInvoice = (invoice) => {
    //setPDF(new PDF());
    const pdf = new PDF();
    var species = [];
    var desc = [];
    var quanitity = [];
    var price = [];
    var amount = [];
    for (var i = 0; i < invoice.orderItems.length; i++) {
      species.push(invoice.orderItems[i].category);
      desc.push(invoice.orderItems[i].description);
      quanitity.push(invoice.orderItems[i].weight.toFixed(3));
      price.push(`$${invoice.orderItems[i].price}`);
      amount.push(`$${invoice.orderItems[i].subtotal.toFixed(2)}`);
    }
    //console.log(species);

    const soldTo = JSON.parse(invoice.soldTo);
    const obj = {
      SoldTo: {
        Name: soldTo.Name,
        Address: soldTo.Address,
        City: soldTo.City,
        State: soldTo.State,
      },
      InvoiceNumber: invoice.invoiceNum,
      InvoiceDate: invoice.date,
      OurOrderNo: '\\',
      YourOrderNo: '1',
      Terms: 'Net 30',
      SalesRep: receiptSalesRep, // TODO: Add to settings
      ShippedVia: '',
      FOB: '',
      PaymentType: invoice.paymentMethod,
      Discount: invoice.discount ? 'Employee' : 'None',
      DiscountRate: invoice.discountRate,
      Invoice: {
        Species: species,
        Description: desc,
        Quantity: quanitity,
        UnitPrice: price,
        Amount: amount,
      },
      Subtotal: invoice.subtotal,
      Tax: invoice.total - invoice.subtotal,
      Total: invoice.total,
      ChecksPayableTo: {
        // TODO: add to settings
        Name: 'VT Foundation',
        Attn: 'Attn: ' + receiptSalesRep,
        Address1: '3470 Litton Reaves Hall',
        Address2: 'Virginia Tech, Blacksburg, Virginia 24061',
      },
      DirectInquiriesTo: {
        // TODO: add to settings
        Name: receiptSalesRep,
        Phone: '540-231-3318',
        Email: 'email: ' + receiptEmail,
      },
    };
    //console.log(obj);
    pdf.setData(obj);
    setPDFDoc(pdf);
    //pdf.printPDF();
    setPDFSrc(pdf.getPDFSrc());
    //console.log('pdf src: ', pdfSrc);
  };

  return (
    <div>
      <Button
        onClick={handleDownloadPDF}
        style={{
          backgroundColor: '#E5751F',
          color: 'white',
          marginBottom: '20px',
        }}
        variant="contained"
      >
        Download{' '}
      </Button>
      <iframe
        hidden={false}
        src={pdfSrc}
        style={{ position: 'relative', width: '100%', height: '85vh' }}
        download={'test.pdf'}
      ></iframe>
    </div>
  );
}
export default PrintInvoice;
