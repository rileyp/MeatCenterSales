import React, { useState } from 'react';
import { Button, Modal, Box, Typography, TextField } from '@mui/material';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import { useEffect } from 'react';

function CategorySetting() {
  const [addModalOpen, setAddModalOpen] = useState(false);
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [editCategoryModalOpen, setEditCategoryModalOpen] = useState(false);
  const [categories, setCategories] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState(null);
  const [categoryId, setCategoryId] = useState('');
  const [name, setName] = useState('');
  const [taxRate, setTaxRate] = useState('');

  const [newCategoryId, setNewCategoryId] = useState('');
  const [newName, setNewName] = useState('');
  const [newTaxRate, setNewTaxRate] = useState('');

  const [confirmationModalOpen, setConfirmationModalOpen] = useState(false);
  const [validationError, setValidationError] = useState('');

  const handleAddCategory = async () => {
    try {
      const resetValidationError = () => {
        setTimeout(() => {
          setValidationError('');
        }, 3000);
      };

      if (
        categoryId.trim() === '' ||
        name.trim() === '' ||
        taxRate.trim() === ''
      ) {
        setValidationError('Please fill out all the fields.');
        resetValidationError();
        return;
      }

      // Validate category ID is only numbers
      if (!/^\d+$/.test(categoryId)) {
        setValidationError('Category ID must contain only numbers.');
        resetValidationError();
        return;
      }

      // Validate tax rate is only numbers
      if (isNaN(parseFloat(taxRate))) {
        setValidationError('Tax rate must be a valid number.');
        resetValidationError();
        return;
      }

      // Check if category ID already exists
      const categoryExists = categories.some(
        (category) => category.categoryId === parseInt(categoryId),
      );
      if (categoryExists) {
        setValidationError('Category ID already exists.');
        resetValidationError();
        return;
      }

      const categoryToSend = {
        categoryId: parseInt(categoryId),
        name: name,
        taxRate: parseFloat(taxRate),
      };
      console.log(categoryToSend);
      const addCategoryResponse =
        await window.electron.addCategory(categoryToSend);
      fetchCategories();
    } catch (error) {
      console.error('Error adding categories:', error);
      //add snackbar Here
    }
    setAddModalOpen(false);
  };

  const handleAddClick = () => {
    //setNewInitials('');
    setAddModalOpen(true);
  };

  useEffect(() => {
    fetchCategories();
  }, []);

  // Function to fetch categories from backend
  const fetchCategories = async () => {
    try {
      const categories = await window.electron.getCategories();
      console.log('all categoris is: ', categories);
      setCategories(categories);
    } catch (error) {
      console.error('Error fetching categories:', error);
    }
  };

  const handleEditClick = (category) => {
    //console.log('handleEditClick set selected category is: ', category);
    //setSelectedCategory(category);
    setEditModalOpen(true);
  };

  const handleEditCategoryClick = (category) => {
    console.log('The edit Category is: ', category);
    setSelectedCategory(category);
    setNewName(category.name);
    setNewCategoryId(category.categoryId);
    setNewTaxRate(category.taxRate);
    setEditCategoryModalOpen(true);
  };

  const handleUpdateCategory = async () => {
    try {
      const newCategory = {
        categoryId: newCategoryId,
        taxRate: newTaxRate,
        name: newName,
      };
      console.log('new category is: ', newCategory);
      const response = await window.electron.updateCategory(newCategory);
      console.log('Category updated sucessfully', response);
      setEditCategoryModalOpen(false);
      fetchCategories(); // Refresh the list of categories after update
    } catch (error) {
      console.error('Error updating category:', error);
    }
  };

  //update later once patrick has updated it
  // const handleDeleteCategory = async (category) => {
  //   try {
  //     await window.electron.deleteCategory(category.categoryId);
  //     console.log('Category deleted successfully');
  //     fetchCategories();
  //   } catch (error) {
  //     console.error('Error deleting category:', error);
  //   }
  // };

  const handleDeleteCategory = async (category) => {
    setSelectedCategory(category);
    setConfirmationModalOpen(true);
  };

  const handleConfirmDelete = async () => {
    try {
      const resetValidationError = () => {
        setTimeout(() => {
          setValidationError('');
        }, 3000);
      };
      console.log('selected category is: ', selectedCategory);
      const categoryDelete = {
        categoryId: selectedCategory.categoryId,
        name: selectedCategory.name,
        taxRate: selectedCategory.taxRate,
      };
      const deleteResponse =
        await window.electron.deleteCategory(categoryDelete);
      console.log('Category deleted successfully');
      fetchCategories();
    } catch (error) {
      console.error('Error deleting category:', error);
      setValidationError('Error deleting category:');
      resetValidationError();
      return;
    }
  };

  return (
    <div
      style={{
        border: '1px solid #ccc',
        borderRadius: '10px',
        padding: '20px',
        marginTop: 40,
        width: '40%',
        margin: '40px auto 0',
      }}
    >
      <Typography variant="h6">Category</Typography>
      <Button
        variant="outlined"
        style={{ marginRight: '10px' }}
        onClick={handleAddClick}
      >
        Add Category
      </Button>
      <Button
        variant="outlined"
        style={{ marginRight: '10px' }}
        onClick={handleEditClick}
      >
        Edit Categories
      </Button>
      <Modal open={addModalOpen} onClose={() => setAddModalOpen(false)}>
        <Box
          sx={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            bgcolor: 'background.paper',
            boxShadow: 24,
            p: 4,
            width: 300,
            textAlign: 'center',
            borderRadius: 2,
          }}
        >
          <Typography variant="h6">Add Category</Typography>
          {validationError && (
            <Alert severity="error" sx={{ mb: 2 }}>
              <AlertTitle>Error</AlertTitle>
              {validationError}
            </Alert>
          )}
          <TextField
            label="Category ID"
            value={categoryId}
            onChange={(e) => setCategoryId(e.target.value)}
            fullWidth
            sx={{ mt: 2, mb: 2 }} // Add margin bottom for spacing
          />
          <TextField
            label="Name"
            value={name}
            onChange={(e) => setName(e.target.value)}
            fullWidth
            sx={{ mb: 2 }} // Add margin bottom for spacing
          />
          <TextField
            label="Tax Rate"
            value={taxRate}
            onChange={(e) => setTaxRate(e.target.value)}
            fullWidth
            sx={{ mb: 2 }} // Add margin bottom for spacing
          />
          <Button
            onClick={handleAddCategory}
            variant="contained"
            sx={{ mt: 2 }}
          >
            Add
          </Button>
        </Box>
      </Modal>
      <Modal open={editModalOpen} onClose={() => setEditModalOpen(false)}>
        <Box
          sx={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            bgcolor: 'background.paper',
            boxShadow: 24,
            p: 4,
            width: 300,
            textAlign: 'center',
            borderRadius: 2,
          }}
        >
          <Typography variant="h6">Edit Categories</Typography>
          {validationError && (
            <Alert severity="error" sx={{ mb: 2 }}>
              <AlertTitle>Error</AlertTitle>
              {validationError}
            </Alert>
          )}
          {categories.map((category) => (
            <div
              key={category.categoryId}
              style={{
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginBottom: '10px',
              }}
            >
              <Typography>{category.name}</Typography>
              <Button
                variant="outlined"
                onClick={() => handleEditCategoryClick(category)}
              >
                Edit
              </Button>
              <Button
                variant="outlined"
                onClick={() => handleDeleteCategory(category)}
                style={{ color: 'red' }}
              >
                Delete
              </Button>
            </div>
          ))}
        </Box>
      </Modal>
      <Modal
        open={editCategoryModalOpen}
        onClose={() => setEditCategoryModalOpen(false)}
      >
        <Box
          sx={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            bgcolor: 'background.paper',
            boxShadow: 24,
            p: 4,
            width: 300,
            textAlign: 'center',
            borderRadius: 2,
          }}
        >
          <Typography variant="h6">Edit Category</Typography>
          {selectedCategory && (
            <div>
              <TextField
                label="Category ID"
                value={newCategoryId}
                onChange={(e) => setNewCategoryId(e.target.value)}
                fullWidth
                sx={{ mt: 2, mb: 2 }} // Add margin bottom for spacing
                disabled
              />
              <TextField
                label="Name"
                value={newName}
                onChange={(e) => setNewName(e.target.value)}
                fullWidth
                sx={{ mb: 2 }} // Add margin bottom for spacing
              />
              <TextField
                label="Tax Rate"
                value={newTaxRate}
                onChange={(e) => setNewTaxRate(e.target.value)}
                fullWidth
                sx={{ mb: 2 }} // Add margin bottom for spacing
              />
              <Button
                onClick={handleUpdateCategory}
                variant="contained"
                sx={{ mt: 2 }}
              >
                Update
              </Button>
            </div>
          )}
        </Box>
      </Modal>
      <Modal
        open={confirmationModalOpen}
        onClose={() => setConfirmationModalOpen(false)}
      >
        <Box
          sx={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            bgcolor: 'background.paper',
            boxShadow: 24,
            p: 4,
            width: 300,
            textAlign: 'center',
            borderRadius: 2,
          }}
        >
          <Typography variant="h6">Confirm Deletion</Typography>
          <Typography variant="body1">
            Are you sure you want to delete the category "
            {selectedCategory && selectedCategory.name}"?
          </Typography>
          <Button
            onClick={() => {
              handleConfirmDelete();
              setConfirmationModalOpen(false);
            }}
            variant="contained"
            color="error"
            sx={{ mt: 2, mr: 1 }}
          >
            Yes
          </Button>
          <Button
            sx={{ mt: 2 }}
            variant="contained"
            onClick={() => setConfirmationModalOpen(false)}
          >
            No
          </Button>
        </Box>
      </Modal>
    </div>
  );
}

export default CategorySetting;
