import React from 'react';
import PropTypes from 'prop-types';
import InventoryCategory from '../components/InventoryCategory';

function ListOfInventoryCategory({ newInventoryData }) {
  return (
    <div>
      {/* <h3>selectedCategory: {JSON.stringify(selectedCategory)}</h3> */}
      {/* <h3>filteredInventoryData: {JSON.stringify(filteredInventoryData)}</h3> */}
      {newInventoryData.map((category, index) => (
        <div key={index} style={{ marginBottom: '50px' }}>
          <InventoryCategory category={category} />
        </div>
      ))}
    </div>
  );
}

export default ListOfInventoryCategory;
