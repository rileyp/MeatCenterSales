import React from 'react';
import ReceiptForm from './ReceiptForm';
import { useLocation } from 'react-router-dom';
import PDF from './CreateInvoice';
import { useState, useEffect } from 'react';
import Button from '@mui/material/Button';
import Modal from '@mui/material/Modal';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import { red } from '@mui/material/colors';
import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  TextField,
  Box,
} from '@mui/material';
import { RadioGroup, FormLabel, Radio, Typography } from '@mui/material';
import CarcassPDF from './CarcassPDF';
import './CarcassInvoices.css';

function CarcassInvoice() {
  const [receiptEmail, setReceiptEmail] = useState('');
  const [receiptSalesRep, setReceiptSalesRep] = useState('');
  const [showPrintModal, setShowPrintModal] = useState(false);
  const [pdfDoc, setPDFDoc] = useState('');
  const [pdfSrc, setPDFSrc] = useState('');
  const [invoiceType, setInvoiceType] = useState('');
  const [carcassWtValue, setCarcassWtValue] = useState('');
  const [slaughterDateValue, setSlaughterDateValue] = useState('');
  const [animalIDValue, setAnimalIDValue] = useState('');
  const [unitPriceValue, setUnitPriceValue] = useState('');
  const [selectedProducts, setSelectedProducts] = useState([]);
  const [errorMsg, setErrorMsg] = useState('');


  const handleDownloadPDF = () => {
    downloadInvoice();
  };

  const handleCompletedPrintPDF = () => {
    setShowPrintModal(false);
  }

  const handleBeefInvoice = () => {
    //const pdf = new CarcassPDF();
    //pdf.downloadPDF();
    setInvoiceType("beef");
    const porkButton = document.getElementById("porkbutton");
    porkButton.style.backgroundColor = "#ffffff";
    const beefButton = document.getElementById("beefbutton");
    beefButton.style.backgroundColor = "#e0e0e0";
  }

  const handlePorkInvoice = () => {
    setInvoiceType("pork");
    const porkButton = document.getElementById("porkbutton");
    porkButton.style.backgroundColor = "#e0e0e0";
    const beefButton = document.getElementById("beefbutton");
    beefButton.style.backgroundColor = "#ffffff";
  }

  const generateInvoice = (data) => {
    const pdf = new CarcassPDF();
    var wts = [];
    var dates = [];
    var ids = [];
    var prices = [];
    var totals = [];
    for (var i = 0; i < data.length; i++) {
      wts.push(data[i].wt);
      dates.push(data[i].date);
      ids.push(data[i].id);
      prices.push(`$${data[i].price}`);
      totals.push(`${data[i].total}`); // no $ on total
    }
    const currDate = new Date();
    console.log(currDate.getDate(), currDate.getFullYear(), currDate.getMonth());
    const invoice = {
        type: invoiceType,
        month: currDate.getMonth(),
        date: currDate.getDate(),
        year: currDate.getFullYear(),
        //invoiceNumber: "09222023-MWB",
        wts: wts,
        dates: dates,
        ids: ids,
        prices: prices,
        totals: totals
    };
    pdf.setData(invoice);
    setPDFDoc(pdf);
    setPDFSrc(pdf.getPDFSrc());
    //pdf.downloadPDF();
  }

  const handleGenerateInvoice = () => {
    setShowPrintModal(true);
    generateInvoice(selectedProducts);
  }

  const isNumber = (n) => {
    return !isNaN(n) && !isNaN(parseFloat(n));
  }

  const handleAddCarcass = () => {
    if (carcassWtValue === "") {
        setErrorMsg("Carcass Weight is a required field");
    } else if (!isNumber(carcassWtValue)) {
        setErrorMsg("Carcass Weight must be a number");
    } else if (unitPriceValue === "") {
        setErrorMsg("Unit Price is a required value");
    } else if (!isNumber(unitPriceValue)) {
        setErrorMsg("Unit Price must be a number");  
    } else {
        setErrorMsg("");
        const totalValue = parseFloat(carcassWtValue) * parseFloat(unitPriceValue);
        const priceValue = parseFloat(unitPriceValue)
        const carcass = {
            wt: carcassWtValue,
            date: slaughterDateValue,
            id: animalIDValue,
            price: priceValue.toFixed(2),
            total: totalValue.toFixed(2)
        }
        console.log(carcass);
        setSelectedProducts((prevSelectedProducts) => [
            ...prevSelectedProducts,
            {
                wt: carcass.wt,
                date: carcass.date,
                id: carcass.id,
                price: carcass.price,
                total: carcass.total,
            },
        ]);
        setCarcassWtValue('');
        setSlaughterDateValue('');
        setAnimalIDValue('');
        setUnitPriceValue('');
    }
  }

  const handleClearInvoice = () => {
    setSelectedProducts([]);
  }

  const handleDeleteInvoiceEntry = (i) => {
    // slice(0) copies the array
    const arr1 = selectedProducts.slice(0); // duplicate array twice, one for each splice
    const arr2 = selectedProducts.slice(0); // this is because splice modifies the array
    const newSelectedProducts = [...arr1.splice(0, i), ...arr2.splice(i + 1)];
    setSelectedProducts(newSelectedProducts);
  };


  const downloadInvoice = () => {
    pdfDoc.doc.save(`${pdfDoc.invoiceNumber}.pdf`);
  };


return (
    <div style={{textAlign: "center"}}>
        <Button variant="outlined" id="beefbutton" onClick={handleBeefInvoice}>Beef</Button>
        <div style={{display: "inline-block", marginLeft: '15px', marginRight: '15px'}}></div>
        <Button variant="outlined" id="porkbutton" onClick={handlePorkInvoice}>Pork</Button>
        <div style={{minHeight: '2em'}}>
            <p class="carcass-error-msg">{errorMsg}</p>
        </div>
        <div hidden={(invoiceType === '')} style={{marginTop: '5px'}}>
            <input
            type="text"
            placeholder="Carcass Weight"
            value={carcassWtValue}
            onChange={(e) => setCarcassWtValue(e.target.value)}
            class="carcassTextEntry"
            />
            <div style={{display: "inline-block", marginLeft: '5px', marginRight: '5px'}}></div>
            <input
            type="text"
            placeholder="Slaughter Date"
            value={slaughterDateValue}
            onChange={(e) => setSlaughterDateValue(e.target.value)}
            class="carcassTextEntry"
            />
            <div style={{display: "inline-block", marginLeft: '5px', marginRight: '5px'}}></div>
            <input
            type="text"
            placeholder="Animal ID"
            value={animalIDValue}
            onChange={(e) => setAnimalIDValue(e.target.value)}
            class="carcassTextEntry"
            />
            <div style={{display: "inline-block", marginLeft: '5px', marginRight: '5px'}}></div>
            <input
            type="text"
            placeholder="Unit Price"
            value={unitPriceValue}
            onChange={(e) => setUnitPriceValue(e.target.value)}
            class="carcassTextEntry"
            />
            <div style={{display: "inline-block", marginLeft: '15px', marginRight: '15px'}}></div>
            <Button onClick={handleAddCarcass}>Add Carcass</Button>
            <div style={{margin: '20px'}}></div>
            {selectedProducts.length > 0 ? (
                <div>
                    <div class="carcass-invoice-table">
                        <ul>
                        <li class="carcass-table-heading">
                            <div class="carcass-heading-wt">Carcass Weight</div>
                            <div class="carcass-heading-date">Slaughter Date</div>
                            <div class="carcass-heading-id">Animal ID</div>
                            <div class="carcass-heading-price">Unit Price</div>
                            <div class="carcass-heading-total">Line Total</div>
                            <div class="carcass-heading-remove"></div>
                        </li>
                        {selectedProducts.map((carcass, index) => (
                            <li key={index}>
                            <div class="carcass-item-weight">
                                {carcass.wt}
                            </div>
                            <div class="carcass-item-date">
                                {carcass.date}
                            </div>
                            <div className="carcass-item-id">
                                {carcass.id}
                            </div>
                            <div class="carcass-item-price">
                                ${carcass.price}
                            </div>
                            <div class="carcass-item-total">
                                ${carcass.total}
                            </div>
                            <div class="carcass-item-remove">
                                <button
                                className="carcass-delete-button"
                                onClick={() => handleDeleteInvoiceEntry(index)}
                                >
                                <DeleteForeverIcon style={{ color: red[500] }} />
                                </button>
                            </div>
                            <li class="carcass-line-sep"></li>
                            </li>
                        ))}
                        </ul>
                    </div>
                    <div class="carcass-cart-detail">

                        <div>
                        <Button onClick={handleClearInvoice}>Clear Invoice</Button>
                        <Button onClick={handleGenerateInvoice}>Generate Invoice</Button>
                        </div>
                    </div>
                </div>
            ) : (<div></div>)}
            
        </div>
        <Modal open={showPrintModal}>
            <Box
            sx={{
                position: 'relative',
                top: '50%',
                left: '50%',
                transform: 'translate(-50%, -50%)',
                bgcolor: 'background.paper',
                boxShadow: 24,
                p: 2,
                width: '80%',
                height: '90%',
                textAlign: 'center',
                borderRadius: 2,
            }}>
                <Typography variant="h6" sx={{ marginBottom: 1 }}>
                Printing Invoice
                </Typography>
                <Button onClick={handleDownloadPDF}>Download </Button>
                <Button onClick={handleCompletedPrintPDF}>Finished/Exit Print</Button>
                <iframe hidden={false} src={pdfSrc} style={{position: 'relative', width: '95%', height: '85%'}} download={"test.pdf"}></iframe>
            </Box>
        

        </Modal>
    </div>
    
);
}
export default CarcassInvoice;

/*                        <div class="total-detail">
                        <span>Subtotal: ${calculateSubtotal()}</span>
                        <span>Total: ${calculateTotal()}</span>
                        </div>
                        */