import { Link } from 'react-router-dom';
import './Footer.css';

const Footer = () => {
  return (
    <footer className="end">
      <div className="footer-left">
      </div>
      <div className="footer-center">
        <p>© 2024 VT Meat Center, all rights reserved</p>
      </div>
      <div className="footer-right">
      </div>
    </footer>
  );
};

export default Footer;
