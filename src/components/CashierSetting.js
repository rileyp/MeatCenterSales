import React, { useState, useEffect } from 'react';
import { Button, Modal, Box, Typography } from '@mui/material';
import { FormControl, InputLabel, IconButton } from '@mui/material';
import { Select, MenuItem, TextField } from '@mui/material';
import Chip from '@mui/material/Chip';
import DeleteIcon from '@mui/icons-material/Delete';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';

function CashierSetting() {
  const [validationError, setValidationError] = useState('');

  const [initials, setInitials] = useState('');
  const [setModalOpen, setSetModalOpen] = useState(false);
  const [addModalOpen, setAddModalOpen] = useState(false);
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [newInitials, setNewInitials] = useState('');
  const [cashierInitials, setCashierInitials] = useState([]);
  const [currInitial, setCurrInitial] = useState('');

  const [currentInitials, setCurrentInitials] = useState('');

  const [selectedCashier, setSelectedCashier] = useState(null); // State to store the selected cashier object

  // State variable for alerts
  const [showAddAlert, setShowAddAlert] = useState(false);
  const [showUpdateAlert, setShowUpdateAlert] = useState(false);
  const [showDeleteAlert, setShowDeleteAlert] = useState(false);

  // Function to load cashier initials from local storage on component mount
  useEffect(() => {
    const storedInitialsString = localStorage.getItem('cashierInitials');
    console.log('stored initial string', storedInitialsString);
    if (storedInitialsString) {
      const storedInitials = JSON.parse(storedInitialsString);
      setCurrInitial(storedInitials);
    }
  }, [initials]);

  //reset these values in order for the modal to be empty
  const handleSetClick = () => {
    setNewInitials('');
    setSelectedCashier('');
    setSetModalOpen(true);
  };

  // Function to handle the change event of the select menu
  const handleSelectChange = (event) => {
    const selectedInitials = event.target.value;
    const selectedCashier = cashierInitials.find(
      (cashier) => cashier.initials === selectedInitials,
    );
    setSelectedCashier(selectedCashier);
  };

  //local storage function to save cashier initials to local storage
  //saving the entire cashier object to local storage to pass id as well
  const handleSaveClick = () => {
    if (selectedCashier) {
      localStorage.setItem('cashierInitials', JSON.stringify(selectedCashier));
      setInitials(selectedCashier.initials);
      setSetModalOpen(false);
      setShowUpdateAlert(true);
      setTimeout(() => {
        setShowUpdateAlert(false);
      }, 2000);
    }
  };

  const handleAddClick = () => {
    setNewInitials('');
    setAddModalOpen(true);
  };

  const handleAddSaveClick = async () => {
    setInitials(newInitials);
    await window.electron.saveCashier(newInitials);
    setAddModalOpen(false);
    setShowAddAlert(true); // Set add alert to be shown
    setTimeout(() => {
      setShowAddAlert(false); // Hide add alert after 2 seconds
    }, 2000);
    fetchCashiers();
  };

  const handleEditClick = () => {
    setEditModalOpen(true);
  };

  const [deleteConfirmationModalOpen, setDeleteConfirmationModalOpen] =
    useState(false);
  const [selectedCashierToDelete, setSelectedCashierToDelete] = useState({});

  const handleDeleteInitial = (cashier) => {
    console.log(cashier);
    setSelectedCashierToDelete(cashier);
    setDeleteConfirmationModalOpen(true);
  };

  // Function to confirm deletion and delete initials
  const handleConfirmDelete = async () => {
    const storedInitialsString = localStorage.getItem('cashierInitials');
    if (storedInitialsString) {
      const storedInitials = JSON.parse(storedInitialsString);
      setCurrInitial(storedInitials);
    }
    console.log(currInitial);
    if (selectedCashierToDelete.cashierId === currInitial.cashierId) {
      setValidationError('Error: cannot delete currently set cashier.');
      return;
    }
    await window.electron.deleteCashier(selectedCashierToDelete.cashierId);
    fetchCashiers();
    // if (initials === selectedCashier) {
    //   setInitials('');
    // }
    setDeleteConfirmationModalOpen(false);
    setEditModalOpen(false);
    setShowDeleteAlert(true);
    setTimeout(() => {
      setShowDeleteAlert(false);
    }, 2000);
  };

  // Hardcoded list of cashier initials for demo purposes
  //use backend here to pull list of initials
  const fetchCashiers = async () => {
    try {
      const cashiers = await window.electron.getCashiers();
      setCashierInitials(cashiers);
      console.log('cashiers are: ', cashierInitials);
    } catch (error) {
      console.error('Error fetching cashiers:', error);
    }
  };
  useEffect(() => {
    fetchCashiers();
  }, []);
  //console.log(cashierInitials);
  return (
    <div
      style={{
        border: '1px solid #ccc',
        borderRadius: '10px',
        padding: '20px',
        marginTop: 40,
        width: '40%',
        margin: '40px auto 0',
      }}
    >
      <Typography variant="h6">Cashiers</Typography>
      <div style={{ display: 'flex' }}>
        <Button
          variant="outlined"
          onClick={handleSetClick}
          style={{ marginRight: '10px' }}
        >
          Set Cashier
        </Button>
        <Button
          variant="outlined"
          onClick={handleAddClick}
          style={{ marginRight: '10px' }}
        >
          Add Cashier
        </Button>
        <Button
          variant="outlined"
          style={{ marginRight: '10px' }}
          onClick={handleEditClick}
        >
          Edit Cashiers
        </Button>
      </div>
      {/* Set Modal */}
      <Modal open={setModalOpen} onClose={() => setSetModalOpen(false)}>
        <Box
          sx={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            bgcolor: 'background.paper',
            boxShadow: 24,
            p: 4,
            width: 300,
            textAlign: 'center',
            borderRadius: 2,
          }}
        >
          <FormControl fullWidth>
            <InputLabel>New Initials</InputLabel>
            <Select
              value={selectedCashier ? selectedCashier.initials : ''}
              onChange={handleSelectChange}
            >
              {cashierInitials.map((cashier) => (
                <MenuItem key={cashier.cashierId} value={cashier.initials}>
                  {cashier.initials}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          <Button onClick={handleSaveClick} variant="contained" sx={{ mt: 2 }}>
            Save
          </Button>
        </Box>
      </Modal>
      {/* Add Modal */}
      <Modal open={addModalOpen} onClose={() => setAddModalOpen(false)}>
        <Box
          sx={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            bgcolor: 'background.paper',
            boxShadow: 24,
            p: 4,
            width: 300,
            textAlign: 'center',
            borderRadius: 2,
          }}
        >
          <TextField
            label="New Initials"
            value={newInitials}
            onChange={(e) => setNewInitials(e.target.value.toUpperCase())}
            inputProps={{ maxLength: 2 }}
            autoFocus
            fullWidth
          />
          <Button
            onClick={handleAddSaveClick}
            variant="contained"
            sx={{ mt: 2 }}
          >
            Save
          </Button>
        </Box>
      </Modal>
      {/* Edit Modal */}
      <Modal open={editModalOpen} onClose={() => setEditModalOpen(false)}>
        <Box
          sx={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            bgcolor: 'background.paper',
            boxShadow: 24,
            p: 4,
            width: 300,
            textAlign: 'center',
            borderRadius: 2,
          }}
        >
          <Typography variant="h6" sx={{ marginBottom: 4 }}>
            Edit Cashiers
          </Typography>
          {validationError && (
            <Alert severity="error" sx={{ mb: 2 }}>
              <AlertTitle>Error</AlertTitle>
              {validationError}
            </Alert>
          )}
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center', // Center items horizontally
              gap: '10px',
            }}
          >
            {cashierInitials.map((cashier, index) => (
              <div
                key={index}
                style={{
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                }} // Center items vertically
              >
                <Typography
                  sx={{ border: '1px solid #ccc', borderRadius: 1, padding: 1 }}
                >
                  {cashier.initials}{' '}
                </Typography>
                {/* Conditionally render delete button */}
                {cashierInitials.length > 1 &&
                  currInitial.initials !== cashier.initials && (
                    <IconButton
                      onClick={() => handleDeleteInitial(cashier)}
                      color="error"
                    >
                      <DeleteIcon />
                    </IconButton>
                  )}
              </div>
            ))}
          </div>
          <Button onClick={() => setEditModalOpen(false)} sx={{ mt: 2 }}>
            Close
          </Button>
        </Box>
      </Modal>
      {/* Delete Confirmation Modal */}
      <Modal
        open={deleteConfirmationModalOpen}
        onClose={() => setDeleteConfirmationModalOpen(false)}
      >
        <Box
          sx={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            bgcolor: 'background.paper',
            boxShadow: 24,
            p: 4,
            width: 300,
            textAlign: 'center',
            borderRadius: 2,
          }}
        >
          <Typography variant="h6">Delete Confirmation</Typography>
          <Typography>Are you sure you want to delete this cashier?</Typography>
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              marginTop: '10px',
            }}
          >
            <Button
              onClick={() => {
                handleConfirmDelete();
                setDeleteConfirmationModalOpen(false);
              }}
              variant="contained"
              color="error"
              sx={{ mr: 2 }}
            >
              Yes
            </Button>
            <Button onClick={() => setDeleteConfirmationModalOpen(false)}>
              No
            </Button>
          </div>
        </Box>
      </Modal>
      {/* Alert for updating initials */}
      {showAddAlert && (
        <Alert
          severity="success"
          variant="outlined"
          onClose={() => setShowAddAlert(false)}
        >
          {`${newInitials} cashier initial has been added!`}
        </Alert>
      )}

      {showUpdateAlert && (
        <Alert
          severity="success"
          variant="outlined"
          onClose={() => setShowUpdateAlert(false)}
        >
          {`${newInitials} cashier initial has been set!`}
        </Alert>
      )}
      {showDeleteAlert && (
        <Alert
          severity="success"
          variant="outlined"
          onClose={() => setShowDeleteAlert(false)}
        >
          {`${selectedCashierToDelete.initials} cashier initial has been deleted!`}
        </Alert>
      )}
    </div>
  );
}
export default CashierSetting;
