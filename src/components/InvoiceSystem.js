import * as React from 'react';
import { useState, useEffect, useRef } from 'react';
import {
  RadioGroup,
  FormLabel,
  Radio,
  Typography,
  Accordion,
} from '@mui/material';
import {
  AccordionSummary,
  AccordionDetails,
  TextField,
  Box,
  Alert,
} from '@mui/material';
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import PersonIcon from '@mui/icons-material/Person';
import FormControlLabel from '@mui/material/FormControlLabel';
import Button from '@mui/material/Button';
import Avatar from '@mui/material/Avatar';
import Chip from '@mui/material/Chip';
import { deepOrange, deepPurple, orange } from '@mui/material/colors';
import Stack from '@mui/material/Stack';
import { useNavigate } from 'react-router-dom';
import PDF from './CreateInvoice.js';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';

import './InvoiceSystem.css';
import { red } from '@mui/material/colors';
import Checkbox from '@mui/material/Checkbox';
import CircularProgress from '@mui/material/CircularProgress';
import Modal from '@mui/material/Modal';
import PrintInvoice from './PrintInvoice.js';
import Snackbar from '@mui/material/Snackbar';
import IconButton from '@mui/material/IconButton';
import MuiAlert from '@mui/material/Alert';
import { green } from '@mui/material/colors';
import CartBadge from './CartBadge.js';

function InvoiceSystem() {
  const timer = React.useRef(setTimeout);

  const navigate = useNavigate();

  const [showPrintModal, setShowPrintModal] = useState(false);
  const [showProductSnackBar, setShowProductSnackBar] = useState(false);
  const [showInvalidCashierSnackbar, setShowInvalidCashierSnackbar] =
    useState(false);

  const [searchValue, setSearchValue] = useState('');
  const [selectedProducts, setSelectedProducts] = useState([]);
  const [finalizeInvoice, setFinalizeInvoice] = useState('');

  const [clerkDailyOrder, setClerkDailyOrder] = useState(1);
  const [currentInitials, setCurrentInitials] = useState('');

  const [receiptEmail, setReceiptEmail] = useState('');
  const [receiptSalesRep, setReceiptSalesRep] = useState('');

  useEffect(() => {
    const storedClerkDailyOrder = localStorage.getItem('clerkDailyOrder');
    //console.log('stored clerk daily order: ', storedClerkDailyOrder);
    if (storedClerkDailyOrder) {
      setClerkDailyOrder(parseInt(storedClerkDailyOrder));
    }
    const storedEmail = localStorage.getItem('receiptEmail');
    const storedSalesRep = localStorage.getItem('receiptSalesRep');

    if (storedEmail) setReceiptEmail(storedEmail);
    if (storedSalesRep) setReceiptSalesRep(storedSalesRep);
  }, []);

  const [expanded, setExpanded] = useState(false);
  const [customerInfo, setCustomerInfo] = useState({
    Name: '',
    Address: '',
    City: '',
    State: '',
  });

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    setCustomerInfo((prevCustomerInfo) => ({
      ...prevCustomerInfo,
      [name]: value,
    }));
  };

  const handleProductSnackBarClose = () => {
    setShowProductSnackBar(false);
  };

  //automatic search
  useEffect(() => {
    console.log(searchValue);
    const handleScan = async () => {
      console.log(searchValue.length);

      if (searchValue.length === 12) {
        // Set a delay before executing handleSearch, messed around o give it a small delay
        setTimeout(() => {
          handleSearch();
        }, 100);
      }
    };

    handleScan();
  }, [searchValue]); // Run the effect whenever searchValue changes

  //using this for auto focus on search Bar
  const searchInputRef = useRef(null);
  useEffect(() => {
    // Focus on the search input when the component mounts
    if (searchInputRef.current) {
      searchInputRef.current.focus();
    }
  }, [showPrintModal]);

  const handleSearch = async () => {
    // Example barcode: 029999701111
    const barcode = searchValue;

    // Drop the leading 0 from the barcode
    //const trimmedBarcode = barcode.substring(1);
    //console.log('trimmed barcode is: ', trimmedBarcode);

    // Extract species, product code, and weight from the barcode
    const species = barcode.substring(0, 1);
    const productCode = barcode.substring(1, 6);
    const weight = parseFloat(barcode.substring(6, 11)) / 1000; // Convert to pounds
    // console.log({
    //   species: species,
    //   productCode: productCode,
    //   weight: weight,
    // });

    try {
      // Send a request to backend to get product information based on the product code
      const product = await window.electron.getProduct(
        parseInt(species + productCode),
      );
      console.log('Product is: ', product);

      if (product) {
        // Calculate total price
        const totalPrice = product.unitPrice * weight;

        // Calculate tax
        const tax = totalPrice * product.category.taxRate;

        setSelectedProducts((prevSelectedProducts) => [
          ...prevSelectedProducts,
          {
            retailCut: product.name,
            pricePerUnit: product.unitPrice,
            quantity: 1,
            weight: weight,
            totalPrice: totalPrice + tax, // add tax to total prices
            taxRate: tax,
            category: product.category,
            productId: product.productId,
            subtotal: totalPrice,
          },
        ]);
        setSearchValue('');
      } else {
        console.log('Product not found!');
      }
    } catch (error) {
      console.error('Error fetching product:', error);
      //document.getElementById('error-msg').textContent = 'Product not found!';
      setShowProductSnackBar(true);
      setTimeout(() => {
        setShowProductSnackBar(false); // Hide add alert after 2 seconds
      }, 2000);
      setSearchValue('');
    }
  };

  const handleCheckingOut = async () => {
    if (!selectedPaymentMethod) {
      setPaymentMethodError(true);
      return;
    }

    fetchCashiers();
    // console.log('list of Cashiers', listOfCashierInitials);
    // console.log('curent cashier: ', initials);

    const isInInitials = listOfCashierInitials.find(
      (obj) =>
        obj.cashierId === currentInitials.cashierId &&
        obj.initials === currentInitials.initials,
    );
    if (!isInInitials) {
      setShowInvalidCashierSnackbar(true);
      return; // Exit the function if cashier is invalid
    }

    //today's date
    const date = new Date().toLocaleDateString();
    const isoDate = new Date(date).toISOString();
    const formattedDate = isoDate.split('T')[0];
    const currentDate = new Date();
    const storedDate = localStorage.getItem('clerkDailyOrderDate');
    let currentClerkDailyOrder = 0;

    if (!storedDate || storedDate !== formattedDate) {
      // If it's a new day, reset the daily order number to 1
      currentClerkDailyOrder = 1;
      //console.log('new day!');
      localStorage.setItem('clerkDailyOrderDate', formattedDate);
    } else {
      // If it's not a new day, increment the current daily order number
      // console.log('current clerk daily order ', currentClerkDailyOrder);
      // console.log('clerk daily order ', clerkDailyOrder);
      setClerkDailyOrder(parseInt(localStorage.getItem('clerkDailyOrder')));
      currentClerkDailyOrder = clerkDailyOrder + 1;
      setClerkDailyOrder(currentClerkDailyOrder);
    }

    // generate unique invoice based on the retail invoice
    const invoiceDate = currentDate
      .toLocaleDateString('en-US', {
        month: '2-digit',
        day: '2-digit',
        year: 'numeric',
      })
      .replace(/\//g, '');
    const invoiceNumber = `R${invoiceDate}-${currentClerkDailyOrder
      .toString()
      .padStart(2, '0')}${initials}`;
    // console.log('invoice num');
    // console.log(invoiceNumber);
    // console.log(JSON.stringify(initials));
    const soldToObject = {
      Name: customerInfo.Name,
      Address: customerInfo.Address,
      City: customerInfo.City,
      State: customerInfo.State,
    };

    // const date = new Date().toLocaleDateString();
    // const isoDate = new Date(date).toISOString();
    // const formattedDate = isoDate.split('T')[0];

    const paymentMethod = selectedPaymentMethod;
    const invoice = {
      date: formattedDate,
      subtotal: parseFloat(calculateSubtotal()),
      total: parseFloat(calculateTotal()),
      clerkDailyOrder: currentClerkDailyOrder, // Use the calculated current clerk daily order
      invoiceNum: invoiceNumber, //unique!!
      ourOrderNum: '',
      yourOrderNum: '',
      terms: '',
      salesRep: receiptSalesRep,
      salesRepEmail: receiptEmail,
      discount: employeeDiscount,
      discountRate: discountRate, // sending a number here
      soldTo: JSON.stringify(soldToObject),
      shippedTo: '',
      paymentMethod: paymentMethod,
      clerkInitials: initials,
      //weight, subtotal, productId
      orderItems: selectedProducts.map((product) => ({
        weight: product.weight,
        subtotal: product.subtotal,
        productId: product.productId,
        category: product.category.name,
        description: product.retailCut,
        quanitity: product.quantity,
        price: product.pricePerUnit,
      })),
    };
    // console.log('seleceted products: ', selectedProducts);
    // console.log('checking out invoice is: ', invoice);
    // console.log('customer info is: ', customerInfo);
    setFinalizeInvoice(invoice);
    setShowPrintModal(true);
  };

  const handleClearTransaction = () => {
    setSelectedProducts([]);
    setSearchValue('');
    setFinalizeInvoice('');
    //setReceiptEmail('');
    //setReceiptSalesRep('');
    setExpanded(false);
    setCustomerInfo({
      Name: '',
      Address: '',
      City: '',
      State: '',
    });
    setSelectedPaymentMethod('');
    setPaymentMethodError(false);
    //setShowModal(false);
    setEmployeeDiscount(false);
    //setDiscountRate('');
    //setInitials('');
    //setClerkId('');
    if (searchInputRef.current) {
      searchInputRef.current.focus();
    }
  };

  const handleNewTransaction = async () => {
    try {
      handleClearTransaction();
      setShowPrintModal(false);
    } catch (error) {
      console.error('Error navigating to home:', error);
    }
  };

  const handleBackButton = async () => {
    setShowPrintModal(false);
    //accounts for when its 1 and above.
    if (clerkDailyOrder >= 1) {
      setClerkDailyOrder(clerkDailyOrder - 1);
    }
    console.log('back out, clerk order is: ', clerkDailyOrder);
  };

  const handleFinalizeTransaction = async () => {
    console.log('current finalize invoice: ', finalizeInvoice);
    try {
      const response = await window.electron.saveTransaction(finalizeInvoice);
      //setFinalizeInvoice(invoice);
      console.log('Transaction saved:', response);
      //console.log(selectedProducts);

      let inventoryResponse;
      selectedProducts.map(async (product) => {
        // Decrease inventory for each product
        inventoryResponse = await window.electron.decreaseInventory(
          product.productId,
          1,
        );
      });
      console.log('Inventory Updated:', inventoryResponse);
      const currentClerkDailyOrder = finalizeInvoice.clerkDailyOrder;
      console.log('in finalize, ', currentClerkDailyOrder);

      // Update the clerk daily order number in local storage if it's not a new day
      if (currentClerkDailyOrder > 1) {
        localStorage.setItem(
          'clerkDailyOrder',
          currentClerkDailyOrder.toString(),
        );
      }
      // // If it's a new day, update the local state with the new order number
      // if (currentClerkDailyOrder === 1) {
      //   setClerkDailyOrder(1);
      // } else {
      //   // If it's not a new day, update the local state with the incremented order number
      //   setClerkDailyOrder(clerkDailyOrder + 1);
      // }
      setShowPrintModal(false);
      handleNewTransaction();
    } catch (error) {
      console.error('Error saving transaction:', error);
    }
  };

  const [discountRate, setDiscountRate] = useState('');

  useEffect(() => {
    // Initialize discountRate state when the component mounts
    setDiscountRate(localStorage.getItem('discountRate') || '');
  }, []);

  const calculateSubtotal = () => {
    //console.log('subtotal:', selectedProducts);
    const sub = selectedProducts.reduce(
      (total, product) => total + product.pricePerUnit * product.weight,
      0,
    );
    var discount = 0;
    if (employeeDiscount) {
      // Retrieve discount rate from local storage and apply if employee discount is enabled
      discount =
        (sub * (parseFloat(localStorage.getItem('discountRate')) || 0)) / 100;
    }
    const subtotal = sub - discount;
    return subtotal.toFixed(2);
  };

  const calculateTotal = () => {
    const discountRate = employeeDiscount
      ? parseFloat(localStorage.getItem('discountRate')) || 0
      : 0;
    const total = selectedProducts.reduce((total, product) => {
      const subtotalPerItem =
        product.pricePerUnit * product.weight * (1 - discountRate / 100);
      const tax = subtotalPerItem * product.category.taxRate;
      return total + subtotalPerItem + tax;
    }, 0);
    return total.toFixed(2);
  };

  const [showModal, setShowModal] = useState(false);
  const [paymentMethodError, setPaymentMethodError] = useState(false);
  const [selectedPaymentMethod, setSelectedPaymentMethod] = useState('');

  const handlePaymentMethodChange = (event) => {
    setSelectedPaymentMethod(event.target.value);
    console.log(event.target.value);
  };

  const handleDeleteInvoiceEntry = (i) => {
    // slice(0) copies the array
    const arr1 = selectedProducts.slice(0); // duplicate array twice, one for each splice
    const arr2 = selectedProducts.slice(0); // this is because splice modifies the array
    const newSelectedProducts = [...arr1.splice(0, i), ...arr2.splice(i + 1)];
    setSelectedProducts(newSelectedProducts);
  };

  const handleInitialsChange = (event) => {
    setSelectedInitials(event?.target?.value);
  };

  const [employeeDiscount, setEmployeeDiscount] = React.useState(false);

  const handleEmployeeDiscount = (e) => {
    setEmployeeDiscount(e.target.checked);
  };

  //section for initials stuff:

  const [initials, setInitials] = useState(''); // State to hold the current cashier initials
  const [clerkId, setClerkId] = useState('');
  const [listOfCashierInitials, setListOfCashierInitials] = useState([]);

  // Function to load cashier initials from local storage on component mount
  useEffect(() => {
    const storedInitialsString = localStorage.getItem('cashierInitials');
    console.log('Stored Initials String:', storedInitialsString);
    fetchCashiers();
    if (storedInitialsString) {
      try {
        const storedInitials = JSON.parse(storedInitialsString);
        // console.log('the stored one here: ', storedInitials);
        // console.log('list of cashier:', listOfCashierInitials);
        // const isInInitials = listOfCashierInitials.find(
        //   (obj) =>
        //     obj.cashierId === storedInitials.cashierId &&
        //     obj.initials === storedInitials.initials,
        // );
        // if (!isInInitials) {
        //   console.log('it is not in initials');
        //   setCurrentInitials('');
        //   setInitials('');
        //   setClerkId('');
        //   return;
        // }
        console.log('Stored Initials:', storedInitials);
        setCurrentInitials(storedInitials);
        setInitials(storedInitials.initials);
        setClerkId(storedInitials.cashierId);
      } catch (error) {
        console.error('Error parsing JSON:', error);
      }
    }
  }, []);

  const fetchCashiers = async () => {
    try {
      const cashiers = await window.electron.getCashiers();
      console.log('cashiers in fetch is:', cashiers);
      setListOfCashierInitials(cashiers);
    } catch (error) {
      console.error('Error fetching cashiers:', error);
    }
  };

  //section to handle scrolling container
  const [scrollingContainerRef, setScrollingContainerRef] = useState(null);

  useEffect(() => {
    // Scroll to the bottom of the scrolling container whenever selectedProducts change
    if (scrollingContainerRef) {
      scrollingContainerRef.scrollTop = scrollingContainerRef.scrollHeight;
    }
  }, [selectedProducts, scrollingContainerRef]);

  return (
    <div
      style={{
        marginBottom: '150px',
      }}
    >
      <div
        style={{
          textAlign: 'center',
          marginTop: '50px',
          marginBottom: '100px',
        }}
      >
        <Chip
          sx={{
            marginRight: 2,
          }}
          avatar={
            <Avatar sx={{ bgcolor: '#E5751F', color: '#ffffff' }}>
              {initials}
            </Avatar>
          }
          label="Clerk"
        />

        <input
          type="text"
          placeholder="Enter Product Code"
          value={searchValue}
          onChange={(e) => setSearchValue(e.target.value)}
          class="product-search-box"
          ref={searchInputRef}
        />
        <button onClick={handleSearch} class="search-button">
          Search
        </button>
        {selectedProducts.length > 0 && (
          <CartBadge itemCount={selectedProducts.length} />
        )}

        <br></br>
        <p id="error-msg"> </p>
      </div>
      {selectedProducts.length > 0 ? (
        <div>
          <div
            class="invoice-table"
            ref={(ref) => setScrollingContainerRef(ref)}
            style={{ overflowY: 'auto', maxHeight: '350px' }}
          >
            <ul>
              <li class="table-heading">
                <div class="heading-retailCut">Retail Cut</div>
                <div class="heading-price">Price / lb</div>
                <div class="heading-quantity">Weight</div>
                <div class="heading-subtotal">Price</div>
                <div class="heading-remove"></div>
              </li>
              {selectedProducts.map((product, index) => (
                <li key={index}>
                  <div class="item-retail-name">{product.retailCut}</div>
                  <div class="item-retail-price">
                    ${product.pricePerUnit.toFixed(2)}
                  </div>
                  <div className="item-retail-quantity">
                    <input
                      type="number"
                      step="0.001"
                      value={product.weight}
                      onChange={(e) => {
                        const newQuantity = parseFloat(e.target.value);
                        if (!isNaN(newQuantity) && newQuantity >= 0) {
                          if (newQuantity === 0) {
                            // 0 we remove
                            const updatedProducts = selectedProducts.filter(
                              (p, idx) => idx !== index,
                            );
                            setSelectedProducts(updatedProducts);
                          } else {
                            // Update the quantity of the item
                            const updatedProducts = selectedProducts.map(
                              (p, idx) => {
                                if (idx === index) {
                                  return {
                                    ...p,
                                    weight: newQuantity,
                                    subtotal: newQuantity * p.pricePerUnit,
                                  };
                                }
                                return p;
                              },
                            );
                            setSelectedProducts(updatedProducts);
                          }
                        }
                      }}
                    />
                  </div>
                  <div class="item-retail-subtotal">
                    ${(product.weight * product.pricePerUnit).toFixed(2)}
                  </div>
                  <div class="item-retail-remove">
                    <button
                      className="delete-button"
                      onClick={() => handleDeleteInvoiceEntry(index)}
                    >
                      <DeleteForeverIcon style={{ color: red[500] }} />
                    </button>
                  </div>
                  <li class="line-sep"></li>
                </li>
              ))}
            </ul>
          </div>
          <div class="cart-detail">
            <FormControl style={{ marginBottom: '5px' }}>
              <FormControlLabel
                control={<Checkbox />}
                onChange={handleEmployeeDiscount}
                label={`Employee Discount (${discountRate}%)`}
              />
            </FormControl>
            <FormControl style={{ marginBottom: '20px' }}>
              <RadioGroup
                row
                aria-label="payment-method"
                name="payment-method"
                value=""
                value={selectedPaymentMethod}
                onChange={handlePaymentMethodChange}
              >
                <FormControlLabel
                  value="Check"
                  control={<Radio />}
                  label="Check"
                />
                <FormControlLabel
                  value="Credit Card"
                  control={<Radio />}
                  label="Credit Card"
                />
              </RadioGroup>
            </FormControl>
            <div class="total-detail">
              <span>Subtotal: ${calculateSubtotal()}</span>
              <span>Total: ${calculateTotal()}</span>
              {paymentMethodError && (
                <Typography style={{ color: 'red' }}>
                  Payment method is required.
                </Typography>
              )}
            </div>
            <div className="accordion-container">
              <Accordion
                expanded={expanded === 'panel1'}
                onChange={handleChange('panel1')}
                sx={{
                  marginBottom: '30px',
                  marginTop: '40px',
                  maxWidth: '600px',
                  backgroundColor: '#ffffff',
                  border: '1px solid #ccc',
                }}
              >
                <AccordionSummary
                  expandIcon={<ExpandMoreIcon />}
                  aria-controls="panel1bh-content"
                  id="panel1bh-header"
                >
                  <Typography>Customer Information</Typography>
                </AccordionSummary>
                <AccordionDetails>
                  <div style={{ display: 'flex', flexDirection: 'column' }}>
                    <TextField
                      label="Name"
                      name="Name"
                      value={customerInfo.Name}
                      onChange={handleInputChange}
                      style={{ marginBottom: '10px', width: '100%' }}
                    />
                    <TextField
                      label="Address"
                      name="Address"
                      value={customerInfo.Address}
                      onChange={handleInputChange}
                      style={{ marginBottom: '10px', width: '100%' }}
                    />
                    <div style={{ display: 'flex', marginBottom: '10px' }}>
                      <TextField
                        label="City"
                        name="City"
                        value={customerInfo.City}
                        onChange={handleInputChange}
                        style={{ marginRight: '10px', flex: 1 }}
                      />
                      <TextField
                        label="State"
                        name="State"
                        value={customerInfo.State}
                        onChange={handleInputChange}
                        style={{ flex: 1 }}
                      />
                    </div>
                  </div>
                </AccordionDetails>
              </Accordion>
            </div>
            <div class="button-container">
              <button class="cart-button" onClick={handleClearTransaction}>
                Clear Transaction
              </button>
              <Box sx={{ display: 'flex', alignItems: 'center' }}>
                <Box sx={{ m: 1, position: 'relative' }}>
                  <button class="checkout-button" onClick={handleCheckingOut}>
                    Proceed to Check Out
                  </button>
                </Box>
              </Box>
            </div>
          </div>
        </div>
      ) : (
        <div class="title-info">
          <h3>Please, begin scanning items.</h3>
          {/* <h3>${JSON.stringify(tst)}</h3> */}
        </div>
      )}
      <Modal open={showPrintModal}>
        <Box
          sx={{
            position: 'relative',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            bgcolor: 'background.paper',
            boxShadow: 24,
            p: 2,
            width: '80%',
            height: '90%',
            textAlign: 'center',
            borderRadius: 2,
            overflowY: 'auto',
            maxHeight: '500vh', // Limit maximum height to 80% of viewport height
          }}
        >
          <Typography variant="h6" sx={{ marginBottom: 1 }}>
            Printing Invoice
          </Typography>
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              marginLeft: '90px',
              gap: '40px', // Add some space between buttons
              marginBottom: '20px',
            }}
          >
            <Button
              onClick={handleBackButton}
              variant="contained"
              style={{ backgroundColor: '#75787b', color: 'white' }}
            >
              Cancel
            </Button>
            <Button
              onClick={handleFinalizeTransaction}
              variant="contained"
              style={{ backgroundColor: '#861F41', color: 'white' }}
            >
              Finalize Transaction
            </Button>
          </div>
          <PrintInvoice invoice={finalizeInvoice}></PrintInvoice>
        </Box>
      </Modal>
      <Snackbar
        open={showProductSnackBar}
        autoHideDuration={4000} // Adjust duration as needed
        onClose={handleProductSnackBarClose}
        message="Product not found!"
      >
        <MuiAlert
          elevation={6}
          variant="filled"
          onClose={handleProductSnackBarClose}
          severity="error"
        >
          Product not found!
        </MuiAlert>
      </Snackbar>
      <Snackbar
        open={showInvalidCashierSnackbar}
        autoHideDuration={4000}
        onClose={() => setShowInvalidCashierSnackbar(false)}
        message="Invalid Cashier: Set Cashiers in Settings"
      >
        <MuiAlert
          elevation={6}
          variant="filled"
          onClose={() => setShowInvalidCashierSnackbar(false)}
          severity="error"
        >
          Invalid Cashier: Set Cashiers in Settings
        </MuiAlert>
      </Snackbar>
    </div>
  );
}

export default InvoiceSystem;
