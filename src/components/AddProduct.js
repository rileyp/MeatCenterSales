import React, { useState, useEffect } from 'react';
import Tooltip from '@mui/material/Tooltip';
import AddIcon from '@mui/icons-material/Add';
import IconButton from '@mui/material/IconButton';
import Modal from '@mui/material/Modal';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import MenuItem from '@mui/material/MenuItem';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/material/Alert';

function AddProduct({ categories, subcategories }) {
  const [successSnackBarOpen, setSuccessSnackBarOpen] = React.useState(false);
  const [sucessSnackBarMessage, setSuccessSnackBarMessage] = React.useState('');
  const handleSuccessSnackbarClose = () => {
    setSuccessSnackBarOpen(false);
  };

  const [errorSnackBarOpen, setErrorSnackBarOpen] = React.useState(false);
  const [errorSnackBarMessage, setErrorSnackBarMessage] = React.useState('');
  const handleErrorSnackbarClose = () => {
    setErrorSnackBarOpen(false);
  };
  const [openAddModal, setOpenAddModal] = useState(false);
  const [newItem, setNewItem] = useState({
    category: '',
    subcategory: '',
    retailCut: '',
    unitPrice: '',
    productCode: '',
    quantity: '',
  });
  const [isAddingNewSubcategory, setIsAddingNewSubcategory] = useState(false);
  const [newSubcategory, setNewSubcategory] = useState('');
  const [validationError, setValidationError] = useState('');
  const [availableSubcategories, setAvailableSubcategories] = useState([]);

  useEffect(() => {
    if (newItem.category !== '') {
      // Call backend function to get subcategories based on selected category
      console.log('new item is: ', newItem);
      // window.electron
      // .getSubcategories(newItem.category.categoryId)
      window.electron
        .getSubcategories(newItem.category)
        .then((subcategories) => {
          setAvailableSubcategories(subcategories);
        })
        .catch((error) => {
          console.error('Error fetching subcategories:', error);
        });
    }
  }, [newItem.category]);

  const handleAddModalOpen = () => {
    setOpenAddModal(true);
  };

  const handleAddModalClose = () => {
    setOpenAddModal(false);
    setIsAddingNewSubcategory(false);
    setValidationError('');
  };

  const handleAddItem = () => {
    setOpenAddModal(true);
  };

  const handleNewItemChange = (prop) => (event) => {
    setNewItem({ ...newItem, [prop]: event.target.value });
  };

  const handleSaveNewItem = async () => {
    console.log('new Item in save:', newItem);
    //handles some validations stuff
    if (
      //newItem.category.name.trim() === '' ||
      newItem.subcategory.trim() === '' ||
      newItem.retailCut.trim() === '' ||
      newItem.unitPrice.trim() === '' ||
      newItem.productCode.trim() === '' ||
      newItem.quantity.trim() === ''
    ) {
      setValidationError('Please fill out all the fields.');
      return;
    }

    if (
      isNaN(parseFloat(newItem.unitPrice)) ||
      isNaN(parseInt(newItem.productCode)) ||
      isNaN(parseInt(newItem.quantity))
    ) {
      setValidationError(
        'Price/Unit, Product Code, and Quantity must be numeric.',
      );
      return;
    }

    // Check if product code is numeric and between 1 and 5 characters
    const productCode = newItem.productCode.trim();
    if (!/^\d{1,5}$/.test(productCode)) {
      setValidationError(
        'Product Code must be numeric and have 1 to 5 characters.',
      );
      return;
    }

    newItem.productCode = productCode.padStart(5, '0');
    console.log('padded prduct code: ', newItem.productCode);
    console.log('New Item:', newItem);
    try {
      const productId = String(newItem.category) + newItem.productCode;
      const productCode = parseInt(newItem.productCode);

      //Constructing the object to send to the backend
      const productToSend = {
        productId: parseInt(productId),
        name: newItem.retailCut,
        unitPrice: parseFloat(newItem.unitPrice),
        productCode: productCode,
        categoryId: newItem.category, // Assuming categoryId is available in category object
        subcategory:
          newSubcategory !== '' ? newSubcategory : newItem.subcategory,
        initialInventory: parseInt(newItem.quantity), // Assuming initialInventory is an integer
      };

      console.log('about to send: ', productToSend);
      // Call backend function to add new item
      const response = await window.electron.addProduct(productToSend);

      console.log('New item added successfully:', response);
      setValidationError('');
      handleAddModalClose();
      setSuccessSnackBarMessage(
        `${productToSend.name} has been added successfully.`,
      );
      setSuccessSnackBarOpen(true);

      // Reset the newItem state to clear the form fields
      setNewItem({
        category: '',
        subcategory: '',
        retailCut: '',
        unitPrice: '',
        productCode: '',
        quantity: '',
      });
      setNewSubcategory('');
    } catch (error) {
      console.error('Error adding new item:', error);
      setErrorSnackBarMessage(`Error adding item. ${error}`);
      setErrorSnackBarOpen(true);
    }
  };

  const handleNewSubcategoryChange = (event) => {
    console.log('the new subcategory is: ', event.target.value);
    setNewSubcategory(event.target.value);
  };

  const handleAddNewSubcategory = () => {
    setIsAddingNewSubcategory(true);
  };

  return (
    <div>
      <Tooltip title="Add new item">
        <IconButton onClick={handleAddItem}>
          <AddIcon />
        </IconButton>
      </Tooltip>
      <Modal
        open={openAddModal}
        onClose={handleAddModalClose}
        aria-labelledby="add-modal-title"
        aria-describedby="add-modal-description"
      >
        <Box
          sx={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            width: 400,
            bgcolor: 'background.paper',
            border: '1px solid #ccc',
            boxShadow: 24,
            p: 4,
            borderRadius: '10px',
          }}
        >
          <Typography variant="h6" id="modal-title" gutterBottom>
            Add New Item
          </Typography>
          {/* Alert for validation error */}
          {validationError && (
            <Alert severity="error" sx={{ mb: 2 }}>
              <AlertTitle>Error</AlertTitle>
              {validationError}
            </Alert>
          )}
          {/* Dropdown for selecting category */}
          <TextField
            select
            label="Category"
            value={newItem.category}
            onChange={handleNewItemChange('category')}
            fullWidth
            margin="normal"
            variant="outlined"
          >
            {categories.map((category) => (
              <MenuItem key={category.categoryId} value={category.categoryId}>
                {category.name}
              </MenuItem>
            ))}
          </TextField>
          {/* Dropdown for selecting subcategory */}
          {!isAddingNewSubcategory ? (
            <TextField
              select
              label="Subcategory"
              value={newItem.subcategory}
              onChange={handleNewItemChange('subcategory')}
              fullWidth
              margin="normal"
              variant="outlined"
            >
              {availableSubcategories.map((subcategory) => (
                <MenuItem key={subcategory} value={subcategory}>
                  {subcategory}
                </MenuItem>
              ))}
              <MenuItem value="New Meat" onClick={handleAddNewSubcategory}>
                Add New Subcategory
              </MenuItem>
            </TextField>
          ) : (
            <TextField
              label="New Subcategory"
              value={newSubcategory}
              onChange={handleNewSubcategoryChange}
              fullWidth
              margin="normal"
              variant="outlined"
            />
          )}
          {/* Input fields for the new item */}
          <TextField
            label="Retail Cut"
            value={newItem.retailCut}
            onChange={handleNewItemChange('retailCut')}
            fullWidth
            margin="normal"
            variant="outlined"
          />
          <TextField
            label="Price/Unit"
            value={newItem.unitPrice}
            onChange={handleNewItemChange('unitPrice')}
            fullWidth
            margin="normal"
            variant="outlined"
          />
          <TextField
            label="Product Code"
            value={newItem.productCode}
            onChange={handleNewItemChange('productCode')}
            fullWidth
            margin="normal"
            variant="outlined"
          />
          <TextField
            label="Quantity (packages)"
            value={newItem.quantity}
            onChange={handleNewItemChange('quantity')}
            fullWidth
            margin="normal"
            variant="outlined"
          />
          {/* Buttons for canceling and saving the new item */}
          <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
            <Button onClick={handleAddModalClose}>Cancel</Button>
            <Button
              onClick={handleSaveNewItem}
              variant="contained"
              color="primary"
            >
              Add
            </Button>
          </Box>
        </Box>
      </Modal>
      <Snackbar
        open={successSnackBarOpen}
        autoHideDuration={6000}
        onClose={handleSuccessSnackbarClose}
      >
        <MuiAlert
          elevation={6}
          variant="filled"
          onClose={handleSuccessSnackbarClose}
          severity="success"
        >
          {sucessSnackBarMessage}
        </MuiAlert>
      </Snackbar>
      <Snackbar
        open={errorSnackBarOpen}
        autoHideDuration={6000}
        onClose={handleErrorSnackbarClose}
      >
        <MuiAlert
          elevation={6}
          variant="filled"
          onClose={handleErrorSnackbarClose}
          severity="error"
        >
          {errorSnackBarMessage}
        </MuiAlert>
      </Snackbar>
    </div>
  );
}
export default AddProduct;
