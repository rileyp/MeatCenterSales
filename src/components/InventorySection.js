import * as React from 'react';
import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { alpha } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import TableSortLabel from '@mui/material/TableSortLabel';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Checkbox from '@mui/material/Checkbox';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import FormControlLabel from '@mui/material/FormControlLabel';
import Switch from '@mui/material/Switch';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import FilterListIcon from '@mui/icons-material/FilterList';
import Modal from '@mui/material/Modal';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import { visuallyHidden } from '@mui/utils';
import Snackbar from '@mui/material/Snackbar';
import MuiAlert from '@mui/material/Alert';
import Alert from '@mui/material/Alert';
import AlertTitle from '@mui/material/AlertTitle';

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === 'desc'
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

// Since 2020 all major browsers ensure sort stability with Array.prototype.sort().
// stableSort() brings sort stability to non-modern browsers (notably IE11). If you
// only support modern browsers you can replace stableSort(exampleArray, exampleComparator)
// with exampleArray.slice().sort(exampleComparator)
function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) {
      return order;
    }
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

const headCells = [
  {
    id: 'name',
    numeric: false,
    disablePadding: true,
    label: 'Retail Cut',
  },
  {
    id: 'unitPrice',
    numeric: true,
    disablePadding: false,
    label: 'Price/Unit',
  },
  {
    id: 'productCode',
    numeric: true,
    disablePadding: false,
    label: 'Product Code',
  },
  {
    id: 'quantity',
    numeric: true,
    disablePadding: false,
    label: 'Quantity (packages)',
  },
];

function EnhancedTableHead(props) {
  const {
    onSelectAllClick,
    order,
    orderBy,
    numSelected,
    rowCount,
    onRequestSort,
  } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        <TableCell padding="checkbox">
          <Checkbox
            color="primary"
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
            inputProps={{
              'aria-label': 'select all desserts',
            }}
          />
        </TableCell>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'right' : 'left'}
            padding={headCell.disablePadding ? 'none' : 'normal'}
            sortDirection={orderBy === headCell.id ? order : false}
            sx={{ fontWeight: 'bold' }}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <Box component="span" sx={visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </Box>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(['asc', 'desc']).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

function EnhancedTableToolbar(props) {
  //needed to pass handleEditItem as a propr bc it exists in inventorySection prop
  const { numSelected, handleEditItem, handleDeleteItem, subCategory } = props;

  return (
    <Toolbar
      sx={{
        pl: { sm: 2 },
        pr: { xs: 1, sm: 1 },
        ...(numSelected > 0 && {
          bgcolor: (theme) =>
            alpha(
              theme.palette.primary.main,
              theme.palette.action.activatedOpacity,
            ),
        }),
      }}
    >
      {numSelected > 0 ? (
        <Typography
          sx={{ flex: '1 1 100%' }}
          color="inherit"
          variant="subtitle1"
          component="div"
        >
          {numSelected} selected
        </Typography>
      ) : (
        <Typography
          sx={{ flex: '1 1 100%', fontWeight: '500' }}
          variant="h5"
          id="tableTitle"
          component="div"
        >
          {subCategory}
        </Typography>
      )}

      {numSelected > 0 ? (
        numSelected === 1 ? (
          <React.Fragment>
            <Tooltip title="Edit">
              <IconButton onClick={handleEditItem}>
                <EditIcon />
              </IconButton>
            </Tooltip>
            <Tooltip title="Delete">
              <IconButton onClick={handleDeleteItem}>
                <DeleteIcon />
              </IconButton>
            </Tooltip>
          </React.Fragment>
        ) : (
          <Tooltip title="Delete">
            <IconButton onClick={handleDeleteItem}>
              <DeleteIcon />
            </IconButton>
          </Tooltip>
        )
      ) : null}
    </Toolbar>
  );
}

EnhancedTableToolbar.propTypes = {
  numSelected: PropTypes.number.isRequired,
};

function InventorySection(props) {
  const rows = props.rows;
  const subCategory = props.subCategory.name;
  const [order, setOrder] = React.useState('asc');
  const [orderBy, setOrderBy] = React.useState('calories');
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [dense, setDense] = React.useState(false);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const [snackbarOpen, setSnackbarOpen] = React.useState(false);
  const [snackbarMessage, setSnackbarMessage] = React.useState('');
  const handleSnackbarClose = () => {
    setSnackbarOpen(false);
  };

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleSelectAllClick = (event) => {
    if (event.target.checked) {
      const newSelected = rows.map((n) => n.inventoryId);
      setSelected(newSelected);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, id) => {
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1),
      );
    }
    setSelected(newSelected);
    console.log(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleChangeDense = (event) => {
    setDense(event.target.checked);
  };

  const isSelected = (id) => selected.indexOf(id) !== -1;

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  //accounting for rows updating!!
  const visibleRows = React.useMemo(
    () =>
      stableSort(rows, getComparator(order, orderBy)).slice(
        page * rowsPerPage,
        page * rowsPerPage + rowsPerPage,
      ),
    [order, orderBy, page, rowsPerPage, rows],
  );

  //modal for editing item
  const [openEditModal, setOpenEditModal] = React.useState(false);
  const [editItem, setEditItem] = React.useState(null);
  const [deleteItem, setDeleteItem] = React.useState(null);

  const handleEditModalClose = () => {
    setOpenEditModal(false);
    setValidationError('');
  };

  //function to implement later
  const handleEditItem = (selectedItems) => {
    // For simplicity, let's assume we'll only edit the first selected item\
    console.log('edited selected items: ', selectedItems);
    const selectedItem = rows.find(
      (row) => row.inventoryId === selectedItems[0],
    );
    if (selectedItem) {
      setEditItem(selectedItem);
      setOpenEditModal(true);
    }
  };

  const [validationError, setValidationError] = useState('');

  const handleSaveEditItem = async () => {
    console.log('editted item before, ', editItem);

    editItem.unitPrice = editItem.unitPrice.toString();
    editItem.productCode = editItem.productCode.toString();
    editItem.quantity = editItem.quantity.toString();

    if (
      editItem.name.trim() === '' ||
      editItem.unitPrice.trim() === '' ||
      editItem.productCode.trim() === '' ||
      editItem.quantity.trim() === ''
    ) {
      setValidationError('Please fill out all the fields.');
      return;
    }

    if (
      isNaN(parseFloat(editItem.unitPrice)) ||
      isNaN(parseInt(editItem.productCode)) ||
      isNaN(parseInt(editItem.quantity))
    ) {
      setValidationError(
        'Price/Unit, Product Code, and Quantity must be numeric.',
      );
      return;
    }

    const productCode = editItem.productCode.trim();
    if (!/^\d{1,5}$/.test(productCode)) {
      setValidationError(
        'Product Code must be numeric and have 1 to 5 characters.',
      );
      return;
    }

    editItem.productCode = productCode.padStart(5, '0');

    try {
      editItem.productId =
        editItem.categoryId.toString() + editItem.productCode;
      console.log('editted item after, ', editItem);

      // Prepare the edited item object to match the structure needed for updateProduct
      const editedProduct = {
        productKey: editItem.productKey,
        productId: parseInt(editItem.productId),
        name: editItem.name, // Assuming retailCut corresponds to name
        unitPrice: parseFloat(editItem.unitPrice), // Ensure unitPrice is parsed as float
        productCode: parseInt(editItem.productCode),
        categoryId: editItem.categoryId, // Ensure you have categoryId in editItem or fetch it from somewhere
        subcategory: editItem.subcategory,
      };

      const editProductInventory = {
        productId: parseInt(editItem.productId),
        quantity: parseInt(editItem.quantity),
      };

      console.log('edited Product to send', editedProduct);
      console.log('edited inventory product:', editProductInventory);

      // Call updateProduct with the prepared edited product
      const updateProduct_Response =
        await window.electron.updateProduct(editedProduct);
      const updateProductInventory_Response =
        await window.electron.updateProductInventory(editProductInventory);

      //fetchRowsData();

      // Show success message
      setSnackbarMessage(`Successfully Edited Item.`);
      setSnackbarOpen(true);

      setSelected([]);

      // Close the edit modal
      handleEditModalClose();
    } catch (error) {
      console.error('Error editing product', error);
      setSnackbarMessage('Error editing item. Please try again later.');
      setSnackbarOpen(true);
    }
  };

  //modal for delete
  const [openDeleteModal, setOpenDeleteModal] = React.useState(false);

  const handleCloseDeleteModal = () => {
    setOpenDeleteModal(false);
  };

  const handleDeleteItem = () => {
    // Use selected array directly instead of finding a single item
    console.log('handle delete item:', rows[0].inventoryId);
    const itemsToDelete = rows.filter((row) =>
      selected.includes(row.inventoryId),
    );
    console.log('itemsToDelete: ', itemsToDelete);
    if (itemsToDelete.length > 0) {
      setDeleteItem(itemsToDelete);
      setOpenDeleteModal(true);
    }
  };

  const handleDeleteSelectedItems = async () => {
    try {
      // Loop through each selected item and delete them one by one
      for (const item of deleteItem) {
        // Implement the delete logic for each item
        console.log('Deleting item:', item);
        const response = await window.electron.deleteProduct(item.productId);
        console.log('Item Deleted', response);
      }
      // Show success message
      setSnackbarMessage(`${deleteItem.length} item(s) deleted successfully.`);
      setSnackbarOpen(true);

      // Clear the selected items after successful deletion
      setSelected([]);

      // After deleting all items, close the delete modal
      handleCloseDeleteModal();
    } catch (error) {
      console.error('Error deleting product', error);
      setSnackbarMessage('Error deleting item. Please try again later.');
      setSnackbarOpen(true);
    }
  };

  return (
    <Box sx={{ width: '100%' }}>
      {/* <h3>--Rows:-- {JSON.stringify(rows)}</h3> */}
      <Paper sx={{ width: '100%', mb: 2, border: '1px solid #ccc' }}>
        <EnhancedTableToolbar
          numSelected={selected.length}
          handleEditItem={() => handleEditItem(selected)}
          handleDeleteItem={() => handleDeleteItem(selected)}
          subCategory={subCategory}
        />

        <TableContainer>
          <Table
            sx={{ minWidth: 750 }}
            aria-labelledby="tableTitle"
            size={dense ? 'small' : 'medium'}
          >
            <EnhancedTableHead
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
            />
            <TableBody>
              {visibleRows.map((row, index) => {
                const isItemSelected = isSelected(row.inventoryId);
                const labelId = `enhanced-table-checkbox-${index}`;

                return (
                  <TableRow
                    hover
                    onClick={(event) => handleClick(event, row.inventoryId)}
                    role="checkbox"
                    aria-checked={isItemSelected}
                    tabIndex={-1}
                    key={row.inventoryId}
                    selected={isItemSelected}
                    sx={{ cursor: 'pointer' }}
                  >
                    <TableCell padding="checkbox">
                      <Checkbox
                        color="primary"
                        checked={isItemSelected}
                        inputProps={{
                          'aria-labelledby': labelId,
                        }}
                      />
                    </TableCell>
                    <TableCell
                      component="th"
                      id={labelId}
                      scope="row"
                      padding="none"
                    >
                      {row.name}
                    </TableCell>
                    <TableCell align="right">
                      ${row.unitPrice.toFixed(2)}
                    </TableCell>
                    <TableCell align="right">{row.productCode}</TableCell>
                    <TableCell align="right">{row.quantity}</TableCell>
                  </TableRow>
                );
              })}
              {emptyRows > 0 && (
                <TableRow
                  style={{
                    height: (dense ? 33 : 53) * emptyRows,
                  }}
                >
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
      <FormControlLabel
        control={<Switch checked={dense} onChange={handleChangeDense} />}
        label="Dense padding"
      />
      <Modal
        open={openEditModal}
        onClose={handleEditModalClose}
        aria-labelledby="edit-modal-title"
        aria-describedby="edit-modal-description"
      >
        <Box
          sx={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            width: 400,
            bgcolor: 'background.paper',
            border: '1px solid #ccc',
            boxShadow: 24,
            p: 4,
            borderRadius: '10px',
          }}
        >
          {/* Editable fields */}
          <Typography variant="h6" id="modal-title" gutterBottom>
            Edit Item
          </Typography>
          {/* Alert for validation error */}
          {validationError && (
            <Alert severity="error" sx={{ mb: 2 }}>
              <AlertTitle>Error</AlertTitle>
              {validationError}
            </Alert>
          )}
          <TextField
            label="Retail Cut"
            value={editItem?.name}
            onChange={(e) => setEditItem({ ...editItem, name: e.target.value })}
            fullWidth
            margin="normal"
            variant="outlined"
          />
          <TextField
            label="Price/Unit"
            value={editItem?.unitPrice}
            onChange={(e) =>
              setEditItem({ ...editItem, unitPrice: e.target.value })
            }
            fullWidth
            margin="normal"
            variant="outlined"
          />
          <TextField
            label="Product Code"
            value={editItem?.productCode}
            onChange={(e) =>
              setEditItem({ ...editItem, productCode: e.target.value })
            }
            fullWidth
            margin="normal"
            variant="outlined"
          />
          <TextField
            label="Quantity (packages)"
            value={editItem?.quantity}
            onChange={(e) =>
              setEditItem({ ...editItem, quantity: e.target.value })
            }
            fullWidth
            margin="normal"
            variant="outlined"
          />
          {/* Save and cancel buttons */}
          <Box sx={{ display: 'flex', justifyContent: 'space-between' }}>
            <Button onClick={handleEditModalClose}>Cancel</Button>
            <Button
              onClick={() => {
                // Save the edited item here
                // Call a function to save the changes to the backend
                handleSaveEditItem();
                // handleEditModalClose();
              }}
              variant="contained"
              color="primary"
            >
              Save
            </Button>
          </Box>
        </Box>
      </Modal>
      <Modal
        open={openDeleteModal}
        onClose={handleCloseDeleteModal}
        aria-labelledby="delete-modal-title"
        aria-describedby="delete-modal-description"
      >
        <Box
          sx={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            width: 400,
            bgcolor: 'background.paper',
            border: '1px solid #ccc',
            boxShadow: 24,
            p: 4,
            borderRadius: '10px',
          }}
        >
          <Typography variant="h6" id="modal-title" gutterBottom>
            Delete Items
          </Typography>
          <Typography variant="body1" id="delete-modal-description">
            Are you sure you want to delete {selected.length} selected item(s)?
          </Typography>
          <Box sx={{ display: 'flex', justifyContent: 'flex-end', mt: 2 }}>
            <Button onClick={handleCloseDeleteModal} sx={{ mr: 2 }}>
              Cancel
            </Button>
            <Button
              onClick={handleDeleteSelectedItems}
              variant="contained"
              color="error"
            >
              Delete
            </Button>
          </Box>
        </Box>
      </Modal>
      <Snackbar
        open={snackbarOpen}
        autoHideDuration={6000}
        onClose={handleSnackbarClose}
      >
        <MuiAlert
          elevation={6}
          variant="filled"
          onClose={handleSnackbarClose}
          severity="success"
        >
          {snackbarMessage}
        </MuiAlert>
      </Snackbar>
    </Box>
  );
}
export default InventorySection;
