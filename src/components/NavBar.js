import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../../assets/images/official-meat-center.png';
import './Navbar.css'; // You can define your styles in a separate CSS file

const pages = ['Invoice', 'Inventory', 'Financial', 'Carcass Invoices', 'Settings'];

function NavBar() {
  return (
    <div className="container">
      <nav className="navbar">
        <div className="logo">
          <Link to="/" className="logo">
            <img src={logo} alt="Logo" />
          </Link>
        </div>
        <div className="nav-links">
          {pages.map((page, index) => (
            <Link
              key={index}
              to={page === 'Invoice' ? '/' : page === 'Carcass Invoices' ? '/carcass_invoices' : `/${page.toLowerCase()}`}
              className="nav-link"
            >
              {page}
            </Link>
          ))}
        </div>
      </nav>
    </div>
  );
}

export default NavBar;
