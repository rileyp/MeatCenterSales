//import { PDFDocument, StandardFonts, rgb } from 'pdf-lib'
import { jsPDF } from 'jspdf'

export default class PDF {
    constructor() {
        //this.doc = PDFDocument.load(fs.readFileSync("invoice_template.pdf", 'utf-8'));
        this.doc = new jsPDF('p', 'in', [8.5, 11]);
        this.metaDataLineHeightFactor = 1.34
        this.invoiceLineHeightFactor = 1.48;
    }

    parseDate(date) {
        const date_time = date.split("T");
        const date_nums = date_time[0].split("-");
        const year = date_nums[0].substring(2); // only get the last 2 nums in year
        const month = date_nums[1];
        const day = date_nums[2];
        const time_nums = date_time[1].split(":");
        var hour = time_nums[0] % 12; // time_nums is 24 hour clock, we want 12
        if (hour === 0) {
            hour = 12;
        }
        const am = time_nums[0] / 12;
        //console.log(am);
        const min = time_nums[1];
        const ampm_text = am < 1? "AM": "PM";
        return `${month}/${day}/${year} ${hour}:${min} ${ampm_text}`
    }

    setData(data) {
        //console.log(data)
        // Sold to stuff
        const soldTo = data.SoldTo;
        this.sellToName = soldTo.Name;
        this.sellToAddress = soldTo.Address;
        this.sellToCity = soldTo.City;
        this.sellToState = soldTo.State;

        // Metadata stuff
        this.invoiceNumber = data.InvoiceNumber;
        //this.time = this.parseDate(data.InvoiceDate);
        this.time = data.InvoiceDate;
        this.ourOrderNo = data.OurOrderNo;
        this.yourOrderNo = data.YourOrderNo;
        this.terms = data.Terms;
        this.salesRepName = data.SalesRep;
        this.shippedVia = data.ShippedVia;
        this.fob = data.FOB;
        if (data.PaymentType === "check" || data.PaymentType === "Check") { // raw data is ugly, make it pretty for invoice
            this.paymentType = "Check";
        } else if (data.PaymentType === "creditCard" || data.PaymentType === "CreditCard" || data.PaymentType === "credit Card" || data.PaymentType === "Credit Card") {
            this.paymentType = "Credit Card";
        } else {
            this.paymentType = "Unknown";
        }
        this.discount = data.Discount;
        this.discountRate = data.DiscountRate;
        //console.log("discount rate");
        //console.log(this.discountRate);

        // Invoice stuff
        //console.log(data.Invoice);
        this.species = data.Invoice.Species;
        this.description = data.Invoice.Description;
        this.quantity = data.Invoice.Quantity;
        this.unitPrice = data.Invoice.UnitPrice;
        this.amount = data.Invoice.Amount;

        // Total stuff
        this.subtotal = data.Subtotal;
        this.total = data.Total;
        this.tax = data.Tax;

        // Footer stuff
        this.payableToName = data.ChecksPayableTo.Name;
        this.payableToAttn = data.ChecksPayableTo.Attn;
        this.payableToAddress1 = data.ChecksPayableTo.Address1;
        this.payableToAddress2 = data.ChecksPayableTo.Address2;
        this.inquiriesName = data.DirectInquiriesTo.Name;
        this.inquiriesPhone = data.DirectInquiriesTo.Phone;
        this.inquiriesEmail = data.DirectInquiriesTo.Email;
        this.notes = data.Notes;

    }

    setSellTo(name, address, city, state) {
        this.sellToName = name;
        this.sellToAddress = address;
        this.sellToCity = city;
        this.sellToState = state;
    }

    generatePDFStructure() {
        this.generateHeader();
        this.generateSoldTo();
        this.generateInvoiceMetaData();
        this.generateTable();
        this.generateFooter();
    }

    generateHeader() {
        // draw header bar
        this.doc.setFillColor("#C00000"); // dark red of header bar
        this.doc.setDrawColor("#C00000");
        this.doc.rect(0.51, 0.61, 7.11, 0.43, 'F');

        // draw VT Meat Center Text
        this.doc.setTextColor("#FFFFFF"); // white
        this.doc.setFontSize(18);
        //console.log(this.doc.getFontList());
        this.doc.setFont("helvetica", "bold");
        this.doc.text("Virginia Tech Meat Center", 0.55, 0.92);

        // draw the INVOICE Text
        this.doc.setFontSize(26);
        this.doc.setFont("times", "normal");
        this.doc.text("INVOICE", 6.09, 0.96);

        // Meat Center Address Info
        this.doc.setFontSize(10);
        this.doc.setFont("helvetica", "normal");
        this.doc.setTextColor("#000000"); // black
        this.doc.text(["360 Duck Pond Dr. FST Building RM# 101", "Virginia Tech, Blacksburg, Virginia 24061                          (540) 231-3318"], 0.53, 1.18);
    }

    generateSoldTo() {
        // Sold to/shipped to headers
        this.doc.setFontSize(8);
        this.doc.setFont("helvetica", "bold");
        this.doc.setTextColor("#000000");
        this.doc.text("SOLD TO:", 0.53, 1.56);
        this.doc.text("SHIPPED TO:", 0.53, 2.5);

        // Text field labels
        this.doc.setFontSize(10);
        this.doc.setFont("helvetica", "normal");
        this.doc.text(["Name", "Address", "City, State"], 0.53, 1.74, {lineHeightFactor: this.metaDataLineHeightFactor});
        this.doc.text(" Same", 0.53, 2.68);

        // Fill in data
        const city_state = this.sellToCity + ", " + this.sellToState;
        this.doc.setFont("helvetica", "bold");
        this.doc.setTextColor("#963634"); // text color from sheet
        this.doc.text([this.sellToName, this.sellToAddress, city_state], 1.55, 1.74, {lineHeightFactor: this.metaDataLineHeightFactor});
    }

    generateInvoiceMetaData() {
        // Labels
        this.doc.setFont("helvetica", "normal");
        this.doc.setFontSize(10);
        this.doc.setTextColor("#000000");
        this.doc.text(["INVOICE NUMBER ", "INVOICE DATE ", "OUR ORDER NO. ", "YOUR ORDER NO. ", "TERMS ", "SALES REP ", "SHIPPED VIA ", "F.O.B. ", "TYPE OF PAYMENT ", "DISCOUNT "], 6.03, 1.77, {lineHeightFactor: this.metaDataLineHeightFactor, align: "right"});

        // Line
        this.doc.setDrawColor("#000000");
        this.doc.setLineWidth(0.015);
        this.doc.line(6.1, 1.61, 6.1, 3.5);

        // Data
        this.doc.text(this.invoiceNumber, 7.60, 1.77, {align: "right", lineHeightFactor: this.metaDataLineHeightFactor});
        this.doc.setFont("helvetica", "bold");
        this.doc.setTextColor("#963634"); // text color from sheet
        //console.log([this.time, this.ourOrderNo, this.yourOrderNo, this.terms, this.salesRepName, this.shippedVia, this.fob, this.paymentType, this.discount])
        this.doc.text([this.time, this.ourOrderNo, this.yourOrderNo, this.terms, this.salesRepName, this.shippedVia, this.fob, this.paymentType, this.discount], 7.60, 1.96, {lineHeightFactor: this.metaDataLineHeightFactor, align: "right"})
    }

    generateTable() {
        const bottom_of_product_section = 8.54;
        const left_edge = 0.51;
        const right_edge = 7.62;
        const bottom_edge = 9.1;
        const top_edge = 3.5;
        // Header
        this.doc.setFillColor("#E3E3E3");
        this.doc.setDrawColor("#000000");
        this.doc.setLineWidth(0.015);
        this.doc.rect(left_edge, top_edge, 7.11, 0.2, 'FD'); // gray bar
        this.doc.rect(left_edge, top_edge, 7.11, 5.6); // big box
        this.doc.line(left_edge, bottom_of_product_section, right_edge, bottom_of_product_section); // notes/subtotal line

        // vertical lines
        this.doc.line(1.54, top_edge, 1.54, bottom_of_product_section);
        this.doc.line(4.02, top_edge, 4.02, bottom_of_product_section);
        this.doc.line(5.11, top_edge, 5.11, bottom_edge);
        this.doc.line(6.1, top_edge, 6.1, bottom_edge);

        // subtotal area lines
        this.doc.line(6.1, bottom_of_product_section + (bottom_edge - bottom_of_product_section) / 3.0, right_edge, bottom_of_product_section + (bottom_edge - bottom_of_product_section) / 3.0);
        this.doc.line(6.1, bottom_of_product_section + (bottom_edge - bottom_of_product_section) / 3.0 * 2, right_edge, bottom_of_product_section + (bottom_edge - bottom_of_product_section) / 3.0 * 2);

        // pay this amount box
        this.doc.rect(6.1, bottom_edge, (right_edge - 6.1), 0.52);
        this.doc.setTextColor("#000000");
        this.doc.setFont("helvetica", "normal");
        this.doc.setFontSize(8);
        this.doc.text(["PAY THIS", "AMOUNT"], (6.1 + right_edge) / 2.0, 9.4, {align: "center", lineHeightFactor: this.metaDataLineHeightFactor});

        // header text
        this.doc.setFont("helvetica", "bold");
        this.doc.setFontSize(8);
        this.doc.text("SPECIES", 1.025, 3.65, {align: "center"});
        this.doc.text("DESCRIPTION", 2.78, 3.65, {align: "center"});
        this.doc.text("QUANTITY", 4.565, 3.65, {align: "center"});
        this.doc.text("UNIT PRICE", 5.605, 3.65, {align: "center"});
        this.doc.text("AMOUNT", 6.86, 3.65, {align: "center"});

        // Table entries
        this.doc.setFont("helvetica", "normal");
        this.doc.setFontSize(10);
        //console.log(this.quantity);
        this.doc.text(this.species, left_edge + 0.025, 3.85, {lineHeightFactor: this.invoiceLineHeightFactor});
        this.doc.text(this.description, 1.54 + 0.025, 3.85, {lineHeightFactor: this.invoiceLineHeightFactor});
        this.doc.text(this.quantity, (4.02 + 5.11)/2.0, 3.85, {lineHeightFactor: this.invoiceLineHeightFactor, align: "center"});
        this.doc.text(this.unitPrice, 6.1 - 0.05, 3.85, {lineHeightFactor: this.invoiceLineHeightFactor, align: "right"});
        this.doc.text(this.amount, right_edge - 0.05, 3.85, {lineHeightFactor: this.invoiceLineHeightFactor, align: "right"});

        // Other text
        this.doc.setFont("helvetica", "normal");
        this.doc.setFontSize(10);
        if (this.discount !== "None" && this.discountRate != null && this.discountRate !== 0) {
            this.doc.text(`Notes: ${this.discount} Discount (${this.discountRate}%)`, 0.535, 8.68);
        } else {
            this.doc.text("Notes:", 0.535, 8.68);
        }
        this.doc.text(["SUBTOTAL", "TAX"], 5.18, 8.7, {lineHeightFactor: 1.1});

        // Total/subtotal area
        this.doc.setFontSize(10);
        this.doc.setTextColor("#000000");
        this.doc.setFont("helvetica", "normal");
        this.doc.text(`$${this.subtotal.toFixed(2)}`, right_edge - 0.05, 8.7, {align: "right"});
        this.doc.text(`$${this.tax.toFixed(2)}`, right_edge - 0.05, 8.86, {align: "right"});
        this.doc.text(`$${this.total.toFixed(2)}`, right_edge - 0.05, 9.24, {align: "right"});
    }

    generateFooter() {
        // Headers
        this.doc.setFont("helvetica", "bold");
        this.doc.setFontSize(10);
        this.doc.setTextColor("#000000");
        this.doc.text("MAKE ALL CHECKS PAYABLE TO:", 0.54, 9.4);
        this.doc.text("DIRECT ALL INQUIRIES TO:", 4.05, 9.4);

        // checks payable to
        this.doc.setFont("helvetica", "normal");
        this.doc.text([this.payableToName, this.payableToAttn, this.payableToAddress1, this.payableToAddress2], 0.54, 9.6, {lineHeightFactor: this.metaDataLineHeightFactor})
        this.doc.text([this.inquiriesName, this.inquiriesPhone, this.inquiriesEmail], 4.05, 9.6, {lineHeightFactor: this.metaDataLineHeightFactor});

        // thank you
        this.doc.setFont("helvetica", "italic");
        this.doc.setFontSize(12);
        this.doc.text("THANK YOU FOR YOUR BUSINESS!", 8.5/2, 10.68, {align: "center"});
    }

    downloadPDF() {
        this.generatePDFStructure();
        this.doc.save(`${this.invoiceNumber}.pdf`);
    }

    async printPDF() {
        this.generatePDFStructure();
        var invoice = document.getElementById("print_invoice");
        //var invoice = document.createElement("iframe")
        const data = await this.doc.output("blob");
        invoice.src = window.URL.createObjectURL(data);
        //console.log(invoice);
        var invoice_window = document.getElementById("print_invoice").contentWindow;
        invoice_window.print();
    }

    getPDFSrc() {
        this.generatePDFStructure();
        var invoice = document.getElementById("print_invoice");
        const data = this.doc.output("dataurlstring", {filename: `${this.invoiceNumber}.pdf`});
        return data; //window.URL.createObjectURL(data);
    }
}

