import React, { useState, useEffect } from 'react';
import {
  Button,
  Typography,
  TextField,
  Box,
  Alert,
  Modal,
} from '@mui/material';

function ReceiptFormSetting() {
  const [receiptEmail, setReceiptEmail] = useState('');
  const [receiptSalesRep, setReceiptSalesRep] = useState('');
  const [emailModalOpen, setEmailModalOpen] = useState(false);
  const [salesRepModalOpen, setSalesRepModalOpen] = useState(false);
  const [emailEditedValue, setEmailEditedValue] = useState('');
  const [repEditedValue, setRepEditedValue] = useState('');

  useEffect(() => {
    const storedEmail = localStorage.getItem('receiptEmail');
    const storedSalesRep = localStorage.getItem('receiptSalesRep');

    if (storedEmail) setReceiptEmail(storedEmail);
    if (storedSalesRep) setReceiptSalesRep(storedSalesRep);
  }, []);

  const saveReceiptEmail = () => {
    localStorage.setItem('receiptEmail', emailEditedValue);
    setReceiptEmail(emailEditedValue);
    setEmailModalOpen(false);
  };

  const saveReceiptSalesRep = () => {
    localStorage.setItem('receiptSalesRep', repEditedValue);
    setReceiptSalesRep(repEditedValue);
    setSalesRepModalOpen(false);
  };

  return (
    <div
      style={{
        border: '1px solid #ccc',
        borderRadius: '10px',
        padding: '20px',
        marginTop: 40,
        width: '40%',
        margin: '40px auto 0',
      }}
    >
      <Typography variant="h6">Receipt Form</Typography>
      <Box sx={{ display: 'flex', alignItems: 'center', marginTop: '10px' }}>
        <TextField
          label="Receipt Email"
          variant="outlined"
          value={receiptEmail}
          disabled
          sx={{ width: '200px' }}
        />
        <Button
          variant="outlined"
          onClick={() => setEmailModalOpen(true)}
          style={{ marginLeft: '10px', height: '50px' }}
        >
          Edit
        </Button>
      </Box>
      <Box sx={{ display: 'flex', alignItems: 'center', marginTop: '25px' }}>
        <TextField
          label="Receipt Sales Rep"
          variant="outlined"
          value={receiptSalesRep}
          disabled
          sx={{ width: '200px' }}
        />
        <Button
          variant="outlined"
          onClick={() => setSalesRepModalOpen(true)}
          style={{ marginLeft: '10px', height: '50px' }}
        >
          Edit
        </Button>
      </Box>
      {/* Modal for editing email */}
      <Modal open={emailModalOpen} onClose={() => setEmailModalOpen(false)}>
        <Box
          sx={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            bgcolor: 'background.paper',
            boxShadow: 24,
            p: 4,
            width: 300,
            textAlign: 'center',
            borderRadius: 2,
          }}
        >
          <TextField
            label="Receipt Email"
            variant="outlined"
            value={emailEditedValue}
            onChange={(e) => setEmailEditedValue(e.target.value)}
            fullWidth
          />
          <Button
            variant="contained"
            onClick={saveReceiptEmail}
            style={{ marginTop: '10px' }}
          >
            Save
          </Button>
        </Box>
      </Modal>
      {/* Modal for editing sales representative */}
      <Modal
        open={salesRepModalOpen}
        onClose={() => setSalesRepModalOpen(false)}
      >
        <Box
          sx={{
            position: 'absolute',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            bgcolor: 'background.paper',
            boxShadow: 24,
            p: 4,
            width: 300,
            textAlign: 'center',
            borderRadius: 2,
          }}
        >
          <TextField
            label="Receipt Sales Rep"
            variant="outlined"
            value={repEditedValue}
            onChange={(e) => setRepEditedValue(e.target.value)}
            fullWidth
          />
          <Button
            variant="contained"
            onClick={saveReceiptSalesRep}
            style={{ marginTop: '10px' }}
          >
            Save
          </Button>
        </Box>
      </Modal>
    </div>
  );
}

export default ReceiptFormSetting;
