import * as React from 'react';
import Badge from '@mui/material/Badge';
import { styled } from '@mui/material/styles';
import IconButton from '@mui/material/IconButton';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';

const StyledBadge = styled(Badge)(({ theme }) => ({
  '& .MuiBadge-badge': {
    right: -3,
    top: -7,
    backgroundColor: '#861F41',
    color: '#FFFFFF',
    border: `2px solid ${theme.palette.background.paper}`,
    padding: '0 4px',
  },
}));

function CartBadge({ itemCount }) {
  return (
    <IconButton aria-label="cart" disabled style={{ marginLeft: '15px' }}>
      <StyledBadge badgeContent={itemCount} color="secondary">
        <ShoppingCartIcon style={{ color: '#75787b' }} />{' '}
      </StyledBadge>
    </IconButton>
  );
}

export default CartBadge;
