import * as React from 'react';
import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import ListOfInventoryCategory from '../components/ListOfInventoryCategory';
import AddProduct from '../components/AddProduct';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';

function Inventory() {
  const [newInventoryData, setNewInventoryData] = React.useState(null);
  const [selectedCategory, setSelectedCategory] = React.useState('All');
  const [categories, setCategories] = React.useState([]);
  const [subcategories, setSubcategories] = React.useState([]);

  // Define categories and subcategories
  const updateSubCategories = (data) => {
    const allSubcategories = data.flatMap((category) =>
      category.subcategories.map((subcategory) => subcategory.name),
    );
    setSubcategories(allSubcategories);
  };

  const handleChangeCategory = (event, newValue) => {
    setSelectedCategory(newValue);
    fetchData();
  };

  const fetchData = async () => {
    try {
      const data = await window.electron.getAllProducts();
      setNewInventoryData(data);
      updateSubCategories(data);
    } catch (error) {
      console.error('Error fetching inventory data:', error);
    }
  };

  const fetchCategories = async () => {
    try {
      const categories = await window.electron.getCategories();
      //console.log('all categoris is: ', categories);
      setCategories(categories);
    } catch (error) {
      console.error('Error fetching categories:', error);
    }
  };

  //with no field this will run each time, here we want to rerender every time selectedCategory changes
  //but what about when we edit or delete
  useEffect(() => {
    fetchData();
    fetchCategories();
  });

  if (!newInventoryData) {
    return <div>Loading...</div>;
  }

  const filteredInventoryData =
    selectedCategory === 'All'
      ? newInventoryData
      : newInventoryData.filter(
          (category) => category.category.name === selectedCategory,
        );

  return (
    <div>
      {/* <h3>${JSON.stringify(categories)}</h3>
      <h3>${JSON.stringify(subcategories)}</h3> */}
      <Tabs
        value={selectedCategory}
        onChange={handleChangeCategory}
        sx={{
          '& .MuiTabs-indicator': {
            backgroundColor: '#861F41',
          },
        }}
      >
        <Tab
          key="All"
          label="All"
          value="All"
          sx={{ '&.Mui-selected': { color: '#861F41' } }}
        />
        {categories.map((category) => (
          <Tab
            key={category.categoryId}
            label={category.name}
            value={category.name} // Changed value to category name
            sx={{ '&.Mui-selected': { color: '#861F41' } }}
          />
        ))}
      </Tabs>
      <div
        style={{
          display: 'flex',
          justifyContent: 'flex-end',
          marginRight: 22,
          marginBottom: 10,
        }}
      >
        <AddProduct categories={categories} subcategories={subcategories} />
      </div>
      <ListOfInventoryCategory
        newInventoryData={filteredInventoryData}
        selectedCategory={selectedCategory}
      />
    </div>
  );
}

export default Inventory;
