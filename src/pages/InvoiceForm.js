import React from 'react';
import ReceiptForm from '../components/ReceiptForm';
import { useLocation } from 'react-router-dom';
import PDF from '../components/CreateInvoice';
import { useState, useEffect } from 'react';

function InvoiceForm() {
  const location = useLocation();
  const invoiceData = location.state.invoiceData;

  const [receiptEmail, setReceiptEmail] = useState('');
  const [receiptSalesRep, setReceiptSalesRep] = useState('');

  //   const [numPages, setNumPages] = useState(null);
  //   const [pageNumber, setPageNumber] = useState(1);
  //   const [showPreview, setShowPreview] = useState(false);
  //   const [pdfBlob, setPdfBlob] = useState(null);

  useEffect(() => {
    const storedEmail = localStorage.getItem('receiptEmail');
    const storedSalesRep = localStorage.getItem('receiptSalesRep');

    if (storedEmail) setReceiptEmail(storedEmail);
    if (storedSalesRep) setReceiptSalesRep(storedSalesRep);
  }, []);

  const handleDownloadPDF = () => {
    printInvoice(invoiceData);
  };

  const handlePreviewPDF = () => {
    previewPdf(invoiceData);
  };

  const onDocumentLoadSuccess = ({ numPages }) => {
    setNumPages(numPages);
  };

  const previewPdf = (invoice) => {
    const pdf = new PDF();
    var species = [];
    var desc = [];
    var quanitity = [];
    var price = [];
    var amount = [];
    for (var i = 0; i < invoice.orderItems.length; i++) {
      species.push(invoice.orderItems[i].species);
      desc.push(invoice.orderItems[i].description);
      quanitity.push(invoice.orderItems[i].quanitity.toFixed(3));
      price.push(`$${invoice.orderItems[i].price}`);
      amount.push(`$${invoice.orderItems[i].amount}`);
    }
    console.log(species);
    const obj = {
      SoldTo: {
        Name: 'Bobby Joe',
        Address: '1000 Temp St',
        City: 'Blacksburg',
        State: 'VA',
      },
      InvoiceNumber: invoice.invoiceNum,
      InvoiceDate: invoice.date,
      OurOrderNo: '\\',
      YourOrderNo: '1',
      Terms: 'Net 30',
      SalesRep: receiptSalesRep, // TODO: Add to settings
      ShippedVia: '',
      FOB: '',
      PaymentType: invoice.paymentMethod,
      Discount: invoice.discount ? 'Employee' : 'None',
      DiscountRate: invoice.discountRate,
      Invoice: {
        Species: species,
        Description: desc,
        Quantity: quanitity,
        UnitPrice: price,
        Amount: amount,
      },
      Subtotal: invoice.subtotal,
      Tax: invoice.total - invoice.subtotal,
      Total: invoice.total,
      ChecksPayableTo: {
        // TODO: add to settings
        Name: 'VT Foundation',
        Attn: 'Attn: ' + receiptSalesRep,
        Address1: '3470 Litton Reaves Hall',
        Address2: 'Virginia Tech, Blacksburg, Virginia 24061',
      },
      DirectInquiriesTo: {
        // TODO: add to settings
        Name: receiptSalesRep,
        Phone: '540-231-3318',
        Email: 'email: ' + receiptEmail,
      },
    };
    console.log(obj);
    pdf.setData(obj);
    const blob = new Blob([pdf.doc.output('blob')], {
      type: 'application/pdf',
    });
    setPdfBlob(blob);
    setShowPreview(true);
  };

  const printInvoice = (invoice) => {
    const pdf = new PDF();
    var species = [];
    var desc = [];
    var quanitity = [];
    var price = [];
    var amount = [];
    for (var i = 0; i < invoice.orderItems.length; i++) {
      species.push(invoice.orderItems[i].species);
      desc.push(invoice.orderItems[i].description);
      quanitity.push(invoice.orderItems[i].quanitity.toFixed(3));
      price.push(`$${invoice.orderItems[i].price}`);
      amount.push(`$${invoice.orderItems[i].amount}`);
    }
    console.log(species);
    const obj = {
      SoldTo: {
        Name: 'Bobby Joe',
        Address: '1000 Temp St',
        City: 'Blacksburg',
        State: 'VA',
      },
      InvoiceNumber: invoice.invoiceNum,
      InvoiceDate: invoice.date,
      OurOrderNo: '\\',
      YourOrderNo: '1',
      Terms: 'Net 30',
      SalesRep: receiptSalesRep, // TODO: Add to settings
      ShippedVia: '',
      FOB: '',
      PaymentType: invoice.paymentMethod,
      Discount: invoice.discount ? 'Employee' : 'None',
      DiscountRate: invoice.discountRate,
      Invoice: {
        Species: species,
        Description: desc,
        Quantity: quanitity,
        UnitPrice: price,
        Amount: amount,
      },
      Subtotal: invoice.subtotal,
      Tax: invoice.total - invoice.subtotal,
      Total: invoice.total,
      ChecksPayableTo: {
        // TODO: add to settings
        Name: 'VT Foundation',
        Attn: 'Attn: ' + receiptSalesRep,
        Address1: '3470 Litton Reaves Hall',
        Address2: 'Virginia Tech, Blacksburg, Virginia 24061',
      },
      DirectInquiriesTo: {
        // TODO: add to settings
        Name: receiptSalesRep,
        Phone: '540-231-3318',
        Email: 'email: ' + receiptEmail,
      },
    };
    console.log(obj);
    pdf.setData(obj);
    pdf.downloadPDF();
  };

  return (
    <div>
      <h3>{JSON.stringify(invoiceData)}</h3>
      <ReceiptForm />
      <button onClick={handleDownloadPDF}>Download Invoice</button>
      {/* <button onClick={handlePreviewPDF}>Preview Invoice</button>
      {showPreview && (
        <div>
          <Document file={pdfBlob} onLoadSuccess={onDocumentLoadSuccess}>
            <Page pageNumber={pageNumber} />
          </Document>
          <p>
            Page {pageNumber} of {numPages}
          </p>
        </div>
      )} */}
    </div>
  );
}
export default InvoiceForm;
