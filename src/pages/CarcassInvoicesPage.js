import React from 'react';
import icon from '../../assets/icon.svg';
import CarcassInvoice from '../components/CarcassInvoices';

function CarcassInvoicePage() {
  return (
    <div style={{textAlign: "center"}}>
      <h1>Carcass Invoices</h1>
      <CarcassInvoice />
    </div>
  );
}
export default CarcassInvoicePage;
