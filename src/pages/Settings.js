import React, { useState, useEffect } from 'react';
import {
  Button,
  Modal,
  Box,
  Typography,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  TextField,
  IconButton,
} from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import CashierSetting from '../components/CashierSetting';
import DiscountSetting from '../components/DiscountSetting';
import ReceiptFormSetting from '../components/ReceiptFormSetting';
import CategorySetting from '../components/CategorySetting';
import { Receipt } from '@mui/icons-material';

function Settings() {
  return (
    <div>
      <Typography
        variant="h5"
        sx={{
          borderBottom: '1px solid #ccc',
          width: '80%',
          paddingBottom: '5px',
          paddingRight: '10px',
        }}
      >
        Settings
      </Typography>

      <div style={{}}>
        <CashierSetting></CashierSetting>
        <CategorySetting></CategorySetting>
        <DiscountSetting></DiscountSetting>
        <ReceiptFormSetting></ReceiptFormSetting>
        <div style={{ marginBottom: '50px' }}></div>
      </div>
    </div>
  );
}

export default Settings;
