import React, { useState } from 'react';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import {
  Accordion,
  AccordionSummary,
  AccordionDetails,
  Button,
  Box,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  Typography,
  Modal,
} from '@mui/material';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import PDF from '../components/CreateInvoice';
import XLSX from "xlsx";

function Financial() {
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);
  const [invoicesByDay, setInvoicesByDay] = useState([]);
  const [showPrintModal, setShowPrintModal] = useState(false);
  const [pdfDoc, setPDFDoc] = useState('');
  const [pdfSrc, setPDFSrc] = useState('');

  const handleDateChange = (date, isStart) => {
    if (isStart) {
      setStartDate(date);
    } else {
      setEndDate(date);
    }
  };

  const handleDownloadPDF = () => {
    downloadInvoice();
  };

  const handlePrintPDF = () => {
    setShowPrintModal(true);
  };

  const handleCompletedPrintPDF = () => {
    setShowPrintModal(false);
  };

  const downloadInvoice = () => {
    pdfDoc.doc.save(`${pdfDoc.invoiceNumber}.pdf`);
  };

  const fetchInvoices = async () => {
    try {
      const invoices = await window.electron.getInvoicesForRange(
        startDate.toISOString().split('T')[0],
        endDate.toISOString().split('T')[0],
      );
      const invoicesByDay = groupInvoicesByDay(invoices);
      setInvoicesByDay(invoicesByDay);
    } catch (error) {
      console.error('Error fetching invoices:', error);
    }
  };

  const downloadCSV = async () => {
    console.log('start is:', startDate.toISOString().split('T')[0]);
    console.log('end  is:', endDate.toISOString().split('T')[0]);
    const report = await window.electron.getExcelReport(startDate.toISOString().split('T')[0], endDate.toISOString().split('T')[0]);
    console.log("report: ", report);
    XLSX.writeFile(report, 'sales_report.xlsx', {compression: true});
  };

  //implement this function once we get function from Patrick
  const handleInvoiceClick = async (invoiceNum) => {
    try {
      const invoice = await window.electron.getInvoice(invoiceNum);
      const pdf = new PDF();
      var species = [];
      var desc = [];
      var quanitity = [];
      var price = [];
      var amount = [];
      for (var i = 0; i < invoice.orderItems.length; i++) {
        species.push(invoice.orderItems[i].category);
        desc.push(invoice.orderItems[i].description);
        quanitity.push(invoice.orderItems[i].weight.toFixed(3));
        price.push(`$${invoice.orderItems[i].price}`);
        amount.push(`$${invoice.orderItems[i].subtotal.toFixed(2)}`);
      }
      const soldTo = JSON.parse(invoice.soldTo);

      const obj = {
        SoldTo: {
          Name: soldTo.Name,
          Address: soldTo.Address,
          City: soldTo.City,
          State: soldTo.State,
        },
        InvoiceNumber: invoice.invoiceNum,
        InvoiceDate: invoice.date,
        OurOrderNo: '\\',
        YourOrderNo: '1',
        Terms: 'Net 30',
        SalesRep: invoice.salesRep, // TODO: Add to settings
        ShippedVia: '',
        FOB: '',
        PaymentType: invoice.paymentMethod,
        Discount: invoice.discount ? 'Employee' : 'None',
        DiscountRate: invoice.discountRate,
        Invoice: {
          Species: species,
          Description: desc,
          Quantity: quanitity,
          UnitPrice: price,
          Amount: amount,
        },
        Subtotal: invoice.subtotal,
        Tax: invoice.total - invoice.subtotal,
        Total: invoice.total,
        ChecksPayableTo: {
          // TODO: add to settings
          Name: 'VT Foundation',
          Attn: 'Attn: ' + invoice.salesRep,
          Address1: '3470 Litton Reaves Hall',
          Address2: 'Virginia Tech, Blacksburg, Virginia 24061',
        },
        DirectInquiriesTo: {
          // TODO: add to settings
          Name: invoice.salesRep,
          Phone: '540-231-3318',
          Email: 'email: ' + invoice.salesRepEmail,
        },
      };
      pdf.setData(obj);
      setPDFDoc(pdf);
      //pdf.printPDF();
      setPDFSrc(pdf.getPDFSrc());
      handlePrintPDF();
    } catch (error) {
      console.error('Error fetching invoice:', error);
    }
  };

  //   const groupInvoicesByDay = (invoices) => {
  //     const invoicesByDay = {};
  //     invoices.forEach((invoice) => {
  //       const date = invoice.date.split('T')[0]; // Extracting only the date part
  //       if (!invoicesByDay[date]) {
  //         invoicesByDay[date] = [];
  //       }
  //       invoicesByDay[date].push(invoice);
  //     });
  //     return invoicesByDay;
  //   };

  const groupInvoicesByDay = (invoices) => {
    const invoicesByDay = {};

    invoices.forEach((invoice) => {
      //doing this just incase but it should already be in the YYYY-MM-DD Format
      const date = invoice.date.split('T')[0]; // Extracting only the date part

      if (!invoicesByDay[date]) {
        invoicesByDay[date] = {
          total: 0,
          subtotal: 0,
          creditTotal: 0,
          checkTotal: 0,
          quantitySold: 0,
          invoices: [],
        };
      }

      invoicesByDay[date].invoices.push(invoice);
      invoicesByDay[date].total += invoice.total;
      invoicesByDay[date].subtotal += invoice.subtotal;

      // Calculate quantity sold, doesn't work rn
      invoice.orderItems.forEach((item) => {
        invoicesByDay[date].quantitySold += item.quantity;
      });

      // For payment method totals, verify this is consistent
      if (invoice.paymentMethod === 'Credit Card') {
        invoicesByDay[date].creditTotal += invoice.total;
      } else if (invoice.paymentMethod === 'Check') {
        invoicesByDay[date].checkTotal += invoice.total;
      }
    });

    return invoicesByDay;
  };

  return (
    <div>
      <LocalizationProvider dateAdapter={AdapterDayjs}>
        {' '}
        <div>
          <div
            style={{
              textAlign: 'center',
              marginTop: '50px',
              marginBottom: '100px',
            }}
          >
            <div style={{ display: 'flex', justifyContent: 'center' }}>
              <DatePicker
                label="Start Date"
                value={startDate}
                onChange={(date) => handleDateChange(date, true)}
                inputFormat="MM/dd/yyyy"
                localeText={{ start: 'Start Date', end: 'End Date' }}
              />
              <div style={{ marginRight: '30px' }}></div>
              <DatePicker
                label="End Date"
                value={endDate}
                onChange={(date) => handleDateChange(date, false)}
                inputFormat="MM/dd/yyyy"
                localeText={{ start: 'Start Date', end: 'End Date' }}
                style={{ marginLeft: '30px' }}
              />
            </div>
            <Button
              onClick={fetchInvoices}
              disabled={!startDate || !endDate}
              variant="contained"
              color="primary"
              style={{
                marginTop: '10px',
              }}
            >
              Fetch Invoices
            </Button>
            <div style={{display: "inline-block", paddingInline: "10px"}}/>
            <Button
              onClick={downloadCSV}
              disabled={!startDate || !endDate}
              variant="contained"
              color="primary"
              style={{
                marginTop: '10px',
              }}
            >
              Download XLSX
            </Button>
          </div>
          {invoicesByDay && Object.keys(invoicesByDay).length > 0 && (
            <div>
              <div
                style={{
                  marginTop: '50px',
                }}
              >
                <Typography
                  variant="h5"
                  gutterBottom
                  style={{
                    marginBottom: '20px',
                    borderBottom: '1px solid #ccc',
                    paddingBottom: '5px',
                    width: '15%',
                  }}
                >
                  Invoice Summary
                </Typography>
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell>Date</TableCell>
                      <TableCell>Total</TableCell>
                      <TableCell>Subtotal</TableCell>
                      <TableCell>Credit Card Total</TableCell>
                      <TableCell>Check Total</TableCell>
                      <TableCell>Total Quantity Sold</TableCell>
                      {/* Add more later if needed  */}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {Object.entries(invoicesByDay).map(([date, invoices]) => (
                      <TableRow key={date}>
                        <TableCell>{date}</TableCell>
                        <TableCell>{invoices.total.toFixed(2)}</TableCell>
                        <TableCell>{invoices.subtotal.toFixed(2)}</TableCell>
                        <TableCell>{invoices.creditTotal.toFixed(2)}</TableCell>
                        <TableCell>{invoices.checkTotal.toFixed(2)}</TableCell>
                        <TableCell>{invoices.quantitySold}</TableCell>
                        {/* Add more details if we want to add different invoices detail  */}
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </div>
              <div
                style={{
                  marginTop: '50px',
                }}
              >
                <Typography
                  variant="h5"
                  gutterBottom
                  style={{
                    marginBottom: '20px',
                    borderBottom: '1px solid #ccc',
                    paddingBottom: '5px',
                    width: '15%',
                  }}
                >
                  Invoice Files
                </Typography>
                {Object.entries(invoicesByDay).map(([date, invoices]) => (
                  <Accordion key={date}>
                    <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                      <Typography>{date}</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                      <Table>
                        <TableHead>
                          <TableRow>
                            <TableCell>Invoice Numbers</TableCell>
                          </TableRow>
                        </TableHead>
                        <TableBody>
                          {invoices.invoices.map((invoice) => (
                            <TableRow key={invoice.invoiceNum}>
                              <TableCell>
                                <Button
                                  onClick={() =>
                                    handleInvoiceClick(invoice.invoiceNum)
                                  }
                                >
                                  {invoice.invoiceNum}
                                </Button>
                              </TableCell>
                            </TableRow>
                          ))}
                        </TableBody>
                      </Table>
                    </AccordionDetails>
                  </Accordion>
                ))}
              </div>
            </div>
          )}
        </div>
      </LocalizationProvider>
      <Modal open={showPrintModal}>
        <Box
          sx={{
            position: 'relative',
            top: '50%',
            left: '50%',
            transform: 'translate(-50%, -50%)',
            bgcolor: 'background.paper',
            boxShadow: 24,
            p: 2,
            width: '80%',
            height: '90%',
            textAlign: 'center',
            borderRadius: 2,
          }}
        >
          <Typography variant="h6" sx={{ marginBottom: 1 }}>
            Printing Invoice
          </Typography>
          <Button onClick={handleDownloadPDF}>Download </Button>
          <Button onClick={handleCompletedPrintPDF}>Finished/Exit Print</Button>
          <iframe
            hidden={false}
            src={pdfSrc}
            style={{ position: 'relative', width: '95%', height: '85%' }}
            download={'test.pdf'}
          ></iframe>
        </Box>
      </Modal>
    </div>
  );
}

export default Financial;
