import React from 'react';
import icon from '../../assets/icon.svg';
import InvoiceSystem from '../components/InvoiceSystem';

function Invoice() {
  return (
    <div className="invoice-container">
      <InvoiceSystem />
    </div>
  );
}
export default Invoice;
