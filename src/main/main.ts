/* eslint global-require: off, no-console: off, promise/always-return: off */

/**
 * This module executes inside of electron's main process. You can start
 * electron renderer process from here and communicate with the other processes
 * through IPC.
 *
 * When running `npm run build` or `npm run build:main`, this file is compiled to
 * `./src/main.js` using webpack. This gives us some performance wins.
 */
import path from 'path';
import { app, BrowserWindow, shell, ipcMain } from 'electron';
import { autoUpdater } from 'electron-updater';
import log from 'electron-log';
import sqlite from 'sqlite3';
import * as fs from 'fs';
import XLSX from 'xlsx';
import MenuBuilder from './menu';
import { resolveHtmlPath } from './util';
import webpackPaths from '../../.erb/configs/webpack.paths';
import * as types from './types';
import * as dbConsts from './dbInit';

const { dialog } = require('electron');

const sqlite3 = sqlite.verbose();

class AppUpdater {
  constructor() {
    log.transports.file.level = 'info';
    autoUpdater.logger = log;
    autoUpdater.checkForUpdatesAndNotify();
  }
}

let mainWindow: BrowserWindow | null = null;

ipcMain.on('ipc-example', async (event, arg) => {
  const msgTemplate = (pingPong: string) => `IPC test: ${pingPong}`;
  console.log(msgTemplate(arg));
  event.reply('ipc-example', msgTemplate('pong'));
});

if (process.env.NODE_ENV === 'production') {
  const sourceMapSupport = require('source-map-support');
  sourceMapSupport.install();
}

const isDebug =
  process.env.NODE_ENV === 'development' || process.env.DEBUG_PROD === 'true';

if (isDebug) {
  require('electron-debug')();
}

function dbInit() {
  const databaseName = 'data.sqlite3';
  const sqlPathDev = path.join(webpackPaths.appPath, 'sql', databaseName);
  const sqlPathProd = path.join(app.getPath('userData'), databaseName);
  const sqlPath = isDebug ? ':memory:' : sqlPathProd;

  const db = new sqlite3.Database(sqlPath, (err) => {
    if (err) console.error('Database opening error: ', err);
  });
  db.exec(dbConsts.tableStrings);
  return db;
}

const db = dbInit();

async function handleDBQuery(query: string): Promise<unknown[]> {
  // const db = new sqlite3.Database(sqlPath, (err) => {
  //   if (err) console.error('Database opening error: ', err);
  // });
  const response = await new Promise((resolve, reject) => {
    db.all(query, (err, rows) => {
      if (err) {
        console.error('Some error with the query: ', err);
        reject(err);
      }
      resolve(rows);
    });
  });
  return response as Promise<unknown[]>;
}

async function getCategories(): Promise<types.Category[]> {
  return handleDBQuery(`SELECT * FROM ${dbConsts.categoryTable}`) as Promise<
    types.Category[]
  >;
}

async function getCategoryById(catId: number): Promise<types.Category> {
  const result = await handleDBQuery(
    `SELECT * from ${dbConsts.categoryTable} WHERE categoryId = ${catId}`,
  );
  if (result.length !== 1) {
    return new Promise((resolve, reject) => {
      reject(new Error('Category not Found!'));
    });
  }
  return new Promise((resolve) => {
    resolve(result[0] as types.Category);
  });
}

async function getSubcategoriesByCategory(catId: number): Promise<string[]> {
  return (
    handleDBQuery(
      `SELECT DISTINCT subcategory from ${dbConsts.productTable} WHERE categoryId = ${catId} ORDER BY subcategory`,
    ) as Promise<unknown[]>
  ).then((value) => value.map((sub) => sub.subcategory));
}

async function getProductById(prodId: number): Promise<types.Product> {
  const result = await handleDBQuery(
    `SELECT productKey, productId, name, unitPrice, categoryId, productCode, subcategory from ${dbConsts.productTable} WHERE productId = ${prodId} and active = 'TRUE'`,
  );
  if (result.length !== 1) {
    return new Promise((resolve, reject) => {
      reject(new Error(`Product not found! ${prodId}`));
    });
  }
  const prod = result[0] as types.DBProduct;
  return getCategoryById(prod.categoryId).then(
    (value) => {
      return new Promise((resolve) => {
        resolve({
          productId: prod.productId,
          productCode: prod.productCode,
          name: prod.name,
          unitPrice: prod.unitPrice,
          category: value,
          subcategory: prod.subcategory,
          productKey: prod.productKey,
        });
      });
    },
    () => {
      return new Promise((resolve, reject) => {
        reject(new Error(`Category for ${prod.name} not found!`));
      });
    },
  );
}

async function getCashiers(): Promise<types.Cashier[]> {
  return handleDBQuery(`SELECT * from ${dbConsts.cashierTable}`) as Promise<
    types.Cashier[]
  >;
}

async function saveCashier(initials: string): Promise<types.Cashier> {
  const ins = db.prepare(
    `INSERT INTO ${dbConsts.cashierTable} (initials) VALUES (?)`,
  );
  const id = await new Promise<number>((resolve, reject) => {
    ins.run(initials, function (error) {
      if (error !== null) {
        console.log(error);
        reject(new Error('Problem Saving cashier to db'));
      }
      resolve(this.lastID);
    });
  });

  return new Promise((resolve) => {
    resolve({
      cashierId: id,
      initials,
    });
  });
}

async function deleteCashier(id: number): Promise<unknown[]> {
  return handleDBQuery(`DELETE FROM ${dbConsts.cashierTable}
    WHERE cashierId = ${id}`);
}

async function getCashierById(cashId: number): Promise<types.Cashier> {
  const result = await handleDBQuery(
    `SELECT * from ${dbConsts.cashierTable} WHERE cashierId = ${cashId}`,
  );
  if (result.length !== 1) {
    return new Promise((resolve, reject) => {
      reject(new Error(`Cashier not Found! ${cashId}`));
    });
  }
  return new Promise((resolve) => {
    resolve(result[0] as types.Cashier);
  });
}

async function getInvoiceByNum(invNum: string): Promise<types.Invoice> {
  const inv = await handleDBQuery(
    `SELECT * FROM ${dbConsts.invoiceTable} WHERE invoiceNum = '${invNum}'`,
  );
  if (inv.length !== 1) {
    return new Promise((resolve, reject) => {
      reject(new Error(`Invoice not found! ${JSON.stringify(invNum)}`));
    });
  }
  const dbInv = inv[0] as types.DBInvoice;
  const invoiceItems = (await handleDBQuery(
    `SELECT * FROM ${dbConsts.invoiceProductTable} where invoiceId = ${dbInv.invoiceId}`,
  )) as types.InvoiceProduct[];
  return new Promise((resolve, reject) => {
    if (invoiceItems.length < 1) {
      reject(new Error(`No products found for this invoice ${invNum}`));
    }
    resolve({
      invoiceId: dbInv.invoiceId,
      date: dbInv.date,
      total: dbInv.total,
      subtotal: dbInv.subtotal,
      clerkInitials: dbInv.clerkInitials,
      clerkDailyOrder: dbInv.clerkDailyOrder,
      invoiceNum: dbInv.invoiceNum,
      orderItems: invoiceItems,
      ourOrderNum: dbInv.ourOrderNum,
      yourOrderNum: dbInv.yourOrderNum,
      terms: dbInv.terms,
      salesRep: dbInv.salesRep,
      discount: dbInv.discount,
      soldTo: dbInv.soldTo,
      shippedTo: dbInv.shippedTo,
      paymentMethod: dbInv.paymentMethod,
      salesRepEmail: dbInv.salesRepEmail,
    });
  });
}

// eslint-disable-next-line prettier/prettier
async function saveTransaction(
  invoice: types.IncomingInvoice,
): Promise<string> {
  const trans = db.prepare(
    `INSERT INTO ${dbConsts.invoiceTable}
    (date, subtotal, total, paymentMethod, clerkInitials, clerkDailyOrder,
      invoiceNum, ourOrderNum, yourOrderNum, terms, salesRep,
      discount, soldTo, shippedTo, salesRepEmail) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
  );
  const id = await new Promise<number>((resolve, reject) => {
    trans.run(
      [
        invoice.date,
        invoice.subtotal,
        invoice.total,
        invoice.paymentMethod,
        invoice.clerkInitials,
        invoice.clerkDailyOrder,
        invoice.invoiceNum,
        invoice.ourOrderNum,
        invoice.yourOrderNum,
        invoice.terms,
        invoice.salesRep,
        invoice.discount,
        invoice.soldTo,
        invoice.shippedTo,
        invoice.salesRepEmail,
      ],
      function (error) {
        if (error !== null) {
          console.log(error);
          // eslint-disable-next-line prettier/prettier
          reject(
            new Error(
              `'Problem Saving transaction to db ${invoice.invoiceNum}`,
            ),
          );
        } else {
          resolve(this.lastID);
        }
      },
    );
  });
  trans.finalize();

  const itemIns = db.prepare(
    `INSERT INTO ${dbConsts.invoiceProductTable}
    (weight, subtotal, invoiceId, productKey, productId, category, description, quantity, price) VALUES (?,?,?,?,?,?,?,?,?)`,
  );

  invoice.orderItems.forEach(async function (item) {
    const prodKey = (
      await handleDBQuery(
        `SELECT productKey FROM ${dbConsts.productTable} WHERE active = 'TRUE' and productId = ${item.productId} LIMIT 1`,
      )
    )[0].productKey as number;
    itemIns.run(
      [
        item.weight,
        item.subtotal,
        id,
        prodKey,
        item.productId,
        item.category,
        item.description,
        item.quanitity,
        item.price,
      ],
      function (error) {
        if (error !== null) {
          console.log(error);
        }
      },
    );
  });

  return new Promise((resolve, reject) => {
    if (id === -1) {
      reject(new Error('Problem Saving transaction to db'));
    }
    resolve(`Invoice id is ${id}`);
  });
}

async function addCategory(cat: types.Category): Promise<string> {
  const add = `INSERT INTO ${dbConsts.categoryTable} (categoryId, name, taxRate)
    VALUES (${cat.categoryId}, '${cat.name}', ${cat.taxRate})`;

  return new Promise<string>((resolve, reject) => {
    db.run(add, function (error) {
      if (error !== null) {
        reject(new Error(`Problem adding new category ${error}`));
      } else {
        resolve(`successful insertion: ${this.lastID}`);
      }
    });
  });
}

async function addProductToInventory(prod: types.NewProduct): Promise<string> {
  const add = `INSERT INTO ${dbConsts.productTable} (productId,
    name, unitPrice, categoryId, productCode, subcategory, active) VALUES
    (${prod.productId}, '${prod.name}', ${prod.unitPrice}, ${prod.categoryId},
      ${prod.productCode}, '${prod.subcategory}', 'TRUE' )`;

  const id = await new Promise<number>((resolve, reject) => {
    db.run(add, function (error) {
      if (error !== null) {
        // eslint-disable-next-line prettier/prettier
        reject(new Error(`'Problem inserting new Product ${error}`));
      } else {
        resolve(this.lastID);
      }
    });
  });

  const invAdd = `INSERT INTO ${dbConsts.inventoryTable}
    (productKey, productId, quantity)
    VALUES (${id}, ${prod.productId}, ${prod.initialInventory})`;

  return new Promise<string>((resolve, reject) => {
    db.run(invAdd, function (error) {
      if (error !== null) {
        // eslint-disable-next-line prettier/prettier
        reject(new Error(`Problem inserting product inventory count ${error}`));
      } else {
        resolve(`successful insertion: ${this.lastID}`);
      }
    });
  });
}

async function deleteProductFromInventory(prodId: number): Promise<unknown[]> {
  await handleDBQuery(`DELETE FROM ${dbConsts.inventoryTable}
    WHERE productId = ${prodId}`);
  return handleDBQuery(`UPDATE ${dbConsts.productTable} SET active = 'FALSE'
    WHERE productId = ${prodId} AND active = 'TRUE'`);
}

async function getProductsByCategory(
  category: number,
): Promise<types.PlainProduct[]> {
  return handleDBQuery(
    `SELECT * FROM ${dbConsts.productTable} WHERE categoryId = ${category} and active = 'TRUE'`,
  ) as Promise<types.PlainProduct[]>;
}

async function updateProduct(prod: types.PlainProduct): Promise<types.Product> {
  return new Promise<string>((resolve, reject) => {
    // This checks to see if the product code + id combo is valid
    const newId =
      String(prod.categoryId) +
      (String(prod.productCode).length === 5
        ? prod.productCode
        : '0'.repeat(5 - String(prod.productCode).length) + prod.productCode);
    if (newId !== String(prod.productId)) {
      reject(new Error('Invalid product id for the product code'));
    }
    db.run(
      `UPDATE ${dbConsts.productTable} SET name = ?, productId = ?, productCode = ?,
      unitPrice = ?, categoryId = ?, subcategory = ? WHERE productKey = ? AND active = 'TRUE'`,
      [
        prod.name,
        prod.productId,
        prod.productCode,
        prod.unitPrice,
        prod.categoryId,
        prod.subcategory,
        prod.productKey,
      ],
      function (error) {
        if (error !== null) {
          // eslint-disable-next-line prettier/prettier
          reject(
            new Error(`'Problem updating product ${prod.productId}: ${error}`),
          );
        } else {
          resolve(`Product updated: ${this.lastID}`);
        }
      },
    );
  })
    .then(() => {
      return handleDBQuery(
        `SELECT productId, productKey FROM ${dbConsts.productTable} WHERE productKey = ${prod.productKey}`,
      );
    })
    .then(async (val) => {
      const p1 = handleDBQuery(
        `UPDATE ${dbConsts.inventoryTable} SET productId = ${val[0].productId} WHERE productKey = ${val[0].productKey}`,
      );
      const p2 = handleDBQuery(
        `UPDATE ${dbConsts.invoiceProductTable} SET productId = ${val[0].productId} WHERE productKey = ${val[0].productKey}`,
      );
      await Promise.all([p1, p2]);
      return getProductById(val[0].productId);
    });
}

async function updateProductInventory(
  prod: types.InventoryCount,
): Promise<string> {
  return new Promise<string>((resolve, reject) => {
    db.run(
      `UPDATE ${dbConsts.inventoryTable} SET quantity = ? WHERE productId = ?`,
      [prod.quantity, prod.productId],
      function (error) {
        if (error !== null) {
          // eslint-disable-next-line prettier/prettier
          reject(
            new Error(
              `'Problem updating inventory ${prod.productId}: ${error}`,
            ),
          );
        } else {
          resolve(`Product updated: ${this.lastID}`);
        }
      },
    );
  });
}

async function decreaseProductInventory(
  prod: number,
  dec: number,
): Promise<string> {
  return new Promise<string>((resolve, reject) => {
    db.run(
      `UPDATE ${dbConsts.inventoryTable} SET quantity = quantity - ? WHERE productId = ?`,
      [dec, prod],
      function (error) {
        if (error !== null) {
          // eslint-disable-next-line prettier/prettier
          reject(new Error(`'Problem updating inventory ${prod}: ${error}`));
        } else {
          resolve(`Product updated: ${this.lastID}`);
        }
      },
    );
  });
}

async function getInventoryByProduct(
  prodId: number,
): Promise<types.InventoryCount> {
  return handleDBQuery(
    `SELECT * FROM ${dbConsts.inventoryTable} WHERE productId = ${prodId}`,
  ).then((value) => {
    const inv = value[0] as types.DBInventory;
    return {
      productId: inv.productId,
      quantity: inv.quantity,
    };
  });
}

async function updateCategory(category: types.Category): Promise<string> {
  return new Promise<string>((resolve, reject) => {
    db.run(
      `UPDATE ${dbConsts.categoryTable} SET name = ?, taxRate = ? WHERE categoryId = ${category.categoryId}`,
      [category.name, category.taxRate],
      function (error) {
        if (error !== null) {
          // eslint-disable-next-line prettier/prettier
          reject(
            new Error(`'Problem updating category ${category.name}: ${error}`),
          );
        } else {
          resolve(`Category updated: ${this.lastID}`);
        }
      },
    );
  });
}

async function deleteCategory(category: types.Category) {
  // Todo, find the category, and then remove it
  // then find all products for the category, and delete them
  const prods = await getProductsByCategory(category.categoryId);
  await handleDBQuery(`DELETE FROM ${dbConsts.categoryTable}
  WHERE categoryId = ${category.categoryId}`);

  return Promise.all(
    prods.map((value) => deleteProductFromInventory(value.productId)),
  );
}

async function getInvoicesForRange(
  start: string,
  end: string,
): Promise<types.Invoice[]> {
  const invoiceNums = `SELECT invoiceNum FROM ${dbConsts.invoiceTable} WHERE
  ('${end}' >= date and '${start}' <= date)`;
  const res = (await handleDBQuery(invoiceNums)).map(
    (v) => (v as object).invoiceNum,
  );

  return Promise.all(
    res.map((inv) => {
      return getInvoiceByNum(inv);
    }),
  );
}

async function getSalesForRange(
  start: string,
  end: string,
): Promise<types.SalesRange> {
  const invoiceCard = `SELECT date, total FROM ${dbConsts.invoiceTable} WHERE
  (paymentMethod == 'Credit Card' and '${end}' >= date and '${start}' <= date)`;
  const invoiceCheck = `SELECT date, total FROM ${dbConsts.invoiceTable} WHERE
  (paymentMethod == 'Check' and '${end}' >= date and '${start}' <= date)`;
  // TODO
  // Get all invoices for the date range that paid with card
  // save list of objs with form {date: '', total: $}
  // Get all invoices for the date range that paid with check
  // save list of objs with form {date: '', total: $}
  // return obj with {card: [], check: []}
  let ret: types.SalesRange = {};
  return handleDBQuery(invoiceCard)
    .then((card) => {
      ret.card = (card as object[]).map((i) => {
        return {
          date: i.date,
          total: i.total,
        };
      });
      return handleDBQuery(invoiceCheck);
    })
    .then((check) => {
      ret.check = (check as object[]).map((j) => {
        return {
          date: j.date,
          total: j.total,
        };
      });
      return ret;
    });
}

async function getAllProducts(): Promise<types.CategoryProducts[]> {
  // TODO Fix this to only look at active products
  const q = `SELECT ${dbConsts.productTable}.productId,
  ${dbConsts.productTable}.productKey,
  ${dbConsts.productTable}.name,
  ${dbConsts.productTable}.unitPrice,
  ${dbConsts.productTable}.categoryId,
  ${dbConsts.productTable}.productCode,
  ${dbConsts.productTable}.subcategory,
  ${dbConsts.inventoryTable}.quantity,
  ${dbConsts.inventoryTable}.inventoryId
  FROM ${dbConsts.productTable} INNER JOIN
  ${dbConsts.inventoryTable} ON ${dbConsts.productTable}.productId = ${dbConsts.inventoryTable}.productId
  WHERE ${dbConsts.productTable}.active = 'TRUE'`;
  const prods = (await handleDBQuery(q)) as types.DBProductCount[];

  const groupBy = <T, K extends keyof any>(arr: T[], key: (i: T) => K) =>
    arr.reduce(
      (groups, item) => {
        (groups[key(item)] ||= []).push(item);
        return groups;
      },
      {} as Record<K, T[]>,
    );

  const prodSplit = groupBy(prods, (i) => i.categoryId);

  const cats = await getCategories();
  const prodStructured = Object.entries(prodSplit).map(
    ([cat, products]) =>
      <types.CategoryProducts>{
        category: cats.find((v) => `${v.categoryId}` === cat),
        subcategories: Object.entries(
          groupBy(products, (i) => i.subcategory),
        ).map(
          ([subcat, subProds]) =>
            <types.SubcategoryProducts>{ name: subcat, products: subProds },
        ),
      },
  );

  prodStructured.sort((a, b) =>
    a.category.name.toUpperCase().localeCompare(b.category.name.toUpperCase()),
  );

  prodStructured.forEach((cat) => {
    cat.subcategories.sort((a, b) =>
      a.name.toUpperCase().localeCompare(b.name.toUpperCase()),
    );
    cat.subcategories.forEach((subCat) => {
      subCat.products.sort((a, b) =>
        a.name.toUpperCase().localeCompare(b.name.toUpperCase()),
      );
    });
  });
  return new Promise<types.CategoryProducts[]>((resolve) => {
    resolve(prodStructured);
  });
}

async function getSalesByProduct(prod: number): Promise<types.ProductSale[]> {
  const prodSales = `SELECT ${dbConsts.invoiceProductTable}.subtotal,
  ${dbConsts.invoiceProductTable}.weight, ${dbConsts.invoiceTable}.date
  FROM ${dbConsts.invoiceProductTable}
  INNER JOIN ${dbConsts.productTable} ON
  ${dbConsts.productTable}.productKey = ${dbConsts.invoiceProductTable}.productKey
  INNER JOIN ${dbConsts.invoiceTable} ON
  ${dbConsts.invoiceTable}.invoiceId = ${dbConsts.invoiceProductTable}.invoiceId
  WHERE ${dbConsts.productTable}.active = 'TRUE'`;

  // QUERY Works
  return handleDBQuery(prodSales) as Promise<types.ProductSale[]>;
}

async function getCategoriesAndSubcategories(): Promise<types.FullCategory[]> {
  const cats = await getCategories();
  let subTkr: { number: string[] } = {};
  await Promise.all(
    cats.map(async (c) => {
      subTkr[c.categoryId] = (
        (await handleDBQuery(
          `SELECT DISTINCT subcategory from ${dbConsts.productTable} where categoryId = ${c.categoryId}`,
        )) as object[]
      ).map((o) => o.subcategory);
    }),
  );

  return new Promise<types.FullCategory[]>((resolve) => {
    resolve(
      cats.map(
        (c) =>
          <types.FullCategory>{
            categoryId: c.categoryId,
            name: c.name,
            taxRate: c.taxRate,
            subcategories: subTkr[c.categoryId],
          },
      ),
    );
  });
}

async function getCSVforRange(start: string, end: string) {
  const invoice = `SELECT date, SUM(total) FROM ${dbConsts.invoiceTable} WHERE
  ('${end}' >= date and '${start}' <= date) GROUP BY date`;
  const cardT = `SELECT date, SUM(total) FROM ${dbConsts.invoiceTable} WHERE
  (paymentMethod = 'Credit Card' and '${end}' >= date and '${start}' <= date)`;
  const checkT = `SELECT date, SUM(total) FROM ${dbConsts.invoiceTable} WHERE
  (paymentMethod = 'Check' and '${end}' >= date and '${start}' <= date)`;
  // [
  //   { date: '2024-03-27', 'SUM(total)': 21.23 },
  //   { date: '2024-04-03', 'SUM(total)': 11.25 },
  //   { date: '2024-04-10', 'SUM(total)': 182.5 }
  // ]
  const dailyTotals = (await handleDBQuery(invoice)) as {
    date: string;
    'SUM(TOTAL)': number;
  }[];
  const cardTotal = (await handleDBQuery(cardT))[0]['SUM(total)'] as number;
  const checkTotal = (await handleDBQuery(checkT))[0]['SUM(total)'] as number;

  const individualSales = await getSalesForRange(start, end);
  const worksheet = XLSX.utils.json_to_sheet(individualSales.check, { origin: 'A2'});
  XLSX.utils.sheet_add_json(worksheet, individualSales.card, { origin: 'F2' });
  XLSX.utils.sheet_add_json(worksheet, dailyTotals, { origin: 'I5' });
  XLSX.utils.sheet_add_aoa(
    worksheet,
    [
      ['Total CC', cardTotal],
      ['Total Check', checkTotal],
    ],
    { origin: 'K5' },
  );
  XLSX.utils.sheet_add_aoa(worksheet, [['Card Sales']], { origin: 'F1' });
  XLSX.utils.sheet_add_aoa(worksheet, [['Date', 'Total']], { origin: 'F2' });
  XLSX.utils.sheet_add_aoa(worksheet, [['Check Sales']], { origin: 'A1' });
  XLSX.utils.sheet_add_aoa(worksheet, [['Date', 'Total']], { origin: 'A2' });
  XLSX.utils.sheet_add_aoa(worksheet, [['Date', 'Total']], { origin: 'I5' });

  const workbook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(workbook, worksheet, 'Sales Report');
  return new Promise<XLSX.WorkBook>((resolve) => {
    resolve(workbook);
  });
}

// This is where the testing insertion begins
async function testAddUpdateDel() {
  console.log('Testing adding products and updating them');
  await addProductToInventory({
    productId: 106011,
    name: 'Ribeye Steak',
    unitPrice: 21.29,
    productCode: 6011,
    categoryId: 1,
    subcategory: 'Steak and Ribs',
    initialInventory: 50,
  });
  const ribeye = await getProductById(106011); // .then((value) =>
  // console.log(JSON.stringify(value)),
  // );
  await updateProduct({
    productKey: ribeye.productKey,
    productId: 109012,
    name: 'EDITRibeye Steak',
    unitPrice: 32.99,
    productCode: 9012,
    categoryId: 1,
    subcategory: 'Steak and Ribs',
  });
  await getProductById(109012); // .then((value) =>
  // console.log(JSON.stringify(value)),
  // );
  await updateProduct({
    productKey: ribeye.productKey,
    productId: 101011,
    name: 'EDITRibeye Steak',
    unitPrice: 32.99,
    productCode: 1211,
    categoryId: 1,
    subcategory: 'Steak and Ribs',
  }).catch((r) => console.log(`caught as expected + ${r}`));

  await updateProduct({
    productKey: ribeye.productKey,
    productId: 106011,
    name: 'EDITRibeye Steak',
    unitPrice: 32.99,
    productCode: 6011,
    categoryId: 1,
    subcategory: 'Steak and Ribs',
  });
  await getProductById(106011);
  await getInventoryByProduct(106011); // .then((value) =>
  // console.log(JSON.stringify(value)),
  // );
  await updateProductInventory({
    productId: 106011,
    quantity: 69420,
  });
  await getInventoryByProduct(106011); // .then((value) =>
  // console.log(JSON.stringify(value)),
  // );
  await decreaseProductInventory(106011, 100); // .then((value) =>
  // console.log(JSON.stringify(value)),
  // );
  await getInventoryByProduct(106011); // .then((value) =>
  // console.log(JSON.stringify(value)),
  // );
  console.log('Updating the categories');
  await addCategory({ categoryId: 101, name: 'test', taxRate: 0.05 });
  await updateCategory({
    categoryId: 101,
    taxRate: 0.01,
    name: 'test2',
  });
  await deleteCategory({ categoryId: 101, name: 'test', taxRate: 0.05 });
  // await getCategories(); // .then((value) =>
  // console.log(JSON.stringify(value) + '\n'),
  // );
  await deleteProductFromInventory(106011);
}
async function testRangeQueries() {
  await getProductsByCategory(1); // .then((value) =>
  // console.log('a' + JSON.stringify(value)),
  // );
  await getSubcategoriesByCategory(1); // .then((value) => console.log(value));

  console.log('Should get us all 3 transactions');
  await getInvoicesForRange(
    new Date('March 27, 2024').toISOString().split('T')[0],
    new Date().toISOString().split('T')[0],
  ).then((value) => console.log(value));

  console.log('Should only get the middle transaction');
  await getInvoicesForRange(
    new Date('March 29, 2024').toISOString().split('T')[0],
    new Date('April 5, 2024').toISOString().split('T')[0],
  ).then((value) => console.log(value));

  console.log('Should give us all the sales');
  await getSalesForRange(
    new Date('March 27, 2024').toISOString().split('T')[0],
    new Date().toISOString().split('T')[0],
  ).then((value) => console.log(JSON.stringify(value)));

  console.log('Should give us all the sales for 1 product');
  await getSalesByProduct(204303).then((value) =>
    console.log(JSON.stringify(value)),
  );
  console.log('Getting all products');
  await getAllProducts().then((v) => console.log(JSON.stringify(v)));
  console.log('Getting all categories');
  await getCategoriesAndSubcategories().then((v) => console.log(v));
}

async function addStarterVals() {
  dbConsts.categories.forEach((v) => addCategory(v));
  dbConsts.products.forEach((v) => addProductToInventory(v));
}

async function debugStuff() {
  const exists = (
    await handleDBQuery(`SELECT COUNT(1) FROM ${dbConsts.productTable}`)
  )[0]['COUNT(1)'];
  console.log(exists);
  if ((exists as number) === 0) {
    await addStarterVals();
  }
  // await insertFakeData(db);
  if (isDebug) {
    const c1 = await saveCashier('CK'); //.catch((reason) => console.log(`a${reason}`));
    const c2 = await saveCashier('PR'); //.catch((reason) => console.log(`a${reason}`));
    const c3 = await saveCashier('UA'); //.catch((reason) => console.log(`a${reason}`));
    await deleteCashier(c3.cashierId).catch((reason) =>
      console.log(`a${reason}`),
    );
    // await getCashierById(1).then((value) => console.log(JSON.stringify(value)));
    // getCategories().then((value) => console.log(JSON.stringify(value)));
    await saveTransaction({
      date: new Date('March 27, 2024 01:30:00').toISOString().split('T')[0],
      total: 21.23,
      subtotal: 20.0,
      clerkDailyOrder: 1,
      invoiceNum: '2024-CK1',
      ourOrderNum: '',
      yourOrderNum: '',
      terms: '',
      salesRep: 'yo mama',
      salesRepEmail: 'asdf@google.com',
      discount: 0,
      soldTo: '',
      shippedTo: '',
      paymentMethod: 'Check',
      clerkInitials: 'CK',
      orderItems: [
        {
          weight: 1.1,
          subtotal: 10.1,
          productId: 110112,
          category: 'test',
          description: 'tesd',
          quanitity: 1,
          price: 0.99,
        },
        {
          weight: 2.1,
          subtotal: 11.1,
          productId: 101031,
          category: 'test',
          description: 'tesd',
          quanitity: 1,
          price: 0.99,
        },
        {
          weight: 2.1,
          subtotal: 11.1,
          productId: 204303,
          category: 'test',
          description: 'tesd',
          quanitity: 1,
          price: 0.99,
        },
        {
          weight: 3.1,
          subtotal: 15.1,
          productId: 204303,
          category: 'test',
          description: 'tesd',
          quanitity: 1,
          price: 0.99,
        },
      ],
    });

    await saveTransaction({
      date: new Date('April 3, 2024 01:30:00').toISOString().split('T')[0],
      total: 11.25,
      subtotal: 20.0,
      clerkDailyOrder: 1,
      invoiceNum: '2024-PR1',
      ourOrderNum: '',
      yourOrderNum: '',
      terms: '',
      salesRepEmail: 'asdf@google.com',
      salesRep: 'yo mama',
      discount: 0,
      soldTo: '',
      shippedTo: '',
      paymentMethod: 'Credit Card',
      clerkInitials: 'PR',
      orderItems: [
        {
          weight: 1.1,
          subtotal: 10.1,
          productId: 110112,
          category: 'test',
          description: 'tesd',
          quanitity: 1,
          price: 0.99,
        },
        {
          weight: 2.1,
          subtotal: 11.1,
          productId: 204303,
          category: 'test',
          description: 'tesd',
          quanitity: 1,
          price: 0.99,
        },
        {
          weight: 3.1,
          subtotal: 15.1,
          productId: 204303,
          category: 'test',
          description: 'tesd',
          quanitity: 1,
          price: 0.99,
        },
      ],
    });
    await saveTransaction({
      date: new Date('April 10, 2024 01:30:00').toISOString().split('T')[0],
      total: 41.25,
      subtotal: 20.0,
      clerkDailyOrder: 1,
      invoiceNum: '2024-UA2',
      ourOrderNum: '',
      yourOrderNum: '',
      terms: '',
      salesRepEmail: 'asdf@google.com',
      salesRep: 'yo mama',
      discount: 0,
      soldTo: '',
      shippedTo: '',
      paymentMethod: 'Credit Card',
      clerkInitials: 'UA',
      orderItems: [
        {
          weight: 1.1,
          subtotal: 10.1,
          productId: 110112,
          category: 'test',
          description: 'tesd',
          quanitity: 1,
          price: 0.99,
        },
        {
          weight: 2.1,
          subtotal: 11.1,
          productId: 101031,
          category: 'test',
          description: 'tesd',
          quanitity: 1,
          price: 0.99,
        },
        {
          weight: 2.1,
          subtotal: 11.1,
          productId: 204303,
          category: 'test',
          description: 'tesd',
          quanitity: 1,
          price: 0.99,
        },
        {
          weight: 5.1,
          subtotal: 15.1,
          productId: 204303,
          category: 'test',
          description: 'tesd',
          quanitity: 1,
          price: 0.99,
        },
      ],
    });
    await saveTransaction({
      date: new Date('April 10, 2024 01:30:00').toISOString().split('T')[0],
      total: 141.25,
      subtotal: 20.0,
      clerkDailyOrder: 1,
      invoiceNum: '2024-UA3',
      ourOrderNum: '',
      yourOrderNum: '',
      terms: '',
      salesRepEmail: 'asdf@google.com',
      salesRep: 'yo mama',
      discount: 0,
      soldTo: '',
      shippedTo: '',
      paymentMethod: 'Credit Card',
      clerkInitials: 'UA',
      orderItems: [
        {
          weight: 1.1,
          subtotal: 10.1,
          productId: 110112,
          category: 'test',
          description: 'tesd',
          quanitity: 1,
          price: 0.99,
        },
        {
          weight: 2.1,
          subtotal: 11.1,
          productId: 101031,
          category: 'test',
          description: 'tesd',
          quanitity: 1,
          price: 0.99,
        },
        {
          weight: 2.1,
          subtotal: 11.1,
          productId: 204303,
          category: 'test',
          description: 'tesd',
          quanitity: 1,
          price: 0.99,
        },
        {
          weight: 5.1,
          subtotal: 15.1,
          productId: 204303,
          category: 'test',
          description: 'tesd',
          quanitity: 1,
          price: 0.99,
        },
      ],
    });
    await getInvoiceByNum('2024-CK1'); //.then((value) => console.log(value));
    await testAddUpdateDel();
    await testRangeQueries();
    await getCSVforRange(
      new Date('January 10, 2024').toISOString().split('T')[0],
      new Date().toISOString().split('T')[0],
    );
    await deleteCashier(c1.cashierId);
    await deleteCashier(c2.cashierId);
    db.all(`DELETE FROM ${dbConsts.invoiceTable};`);
    db.all(`DELETE FROM ${dbConsts.invoiceProductTable};`);
    db.all(
      `UPDATE SQLITE_SEQUENCE SET SEQ=0 WHERE NAME='${dbConsts.cashierTable}'`,
    );
  }
  //hardcoded for lexie butler
  await saveCashier('LB');
}

debugStuff();
// getCategoryById(1001).then((value) => {
//   'c' + console.log(value);
// });
// getProductById(10004).then((value) => {
//   'p' + console.log(value);
// });
// console.log(JSON.stringify(getCategoryById(1001)));
// console.log(JSON.stringify(getProductById(10004)));

// This is where the mock data insertion ends

ipcMain.handle('genericDBQuery', async (event, query) => handleDBQuery(query));
ipcMain.handle('getCategories', async () => getCategories());
ipcMain.handle('getCategoryById', async (event, id) => getCategoryById(id));
ipcMain.handle('getSubcategories', async (event, cat) =>
  getSubcategoriesByCategory(cat),
);
ipcMain.handle('getProductById', async (event, id) => getProductById(id));
ipcMain.handle('getCashiers', async () => getCashiers());
ipcMain.handle('saveCashier', async (event, initials) => saveCashier(initials));
ipcMain.handle('deleteCashier', async (event, id) => deleteCashier(id));
ipcMain.handle('getCashierById', async (event, id) => getCashierById(id));
ipcMain.handle('getInvoiceByNum', async (event, num) => getInvoiceByNum(num));
ipcMain.handle('saveTransaction', async (event, invoice) =>
  saveTransaction(invoice),
);
ipcMain.handle('addCategory', async (event, category) => addCategory(category));
ipcMain.handle('addProductToInventory', async (event, product) =>
  addProductToInventory(product),
);
ipcMain.handle('deleteProductFromInventory', async (event, id) =>
  deleteProductFromInventory(id),
);
ipcMain.handle('getProductsByCategory', async (event, category) =>
  getProductsByCategory(category),
);
ipcMain.handle('updateProduct', async (event, prod) => updateProduct(prod));
ipcMain.handle('updateProductInventory', async (event, prod) =>
  updateProductInventory(prod),
);
ipcMain.handle('decreaseInventory', async (event, prod, dec) =>
  decreaseProductInventory(prod, dec),
);
ipcMain.handle('getInventoryByProduct', async (event, prod) =>
  getInventoryByProduct(prod),
);
ipcMain.handle('updateCategory', async (event, cat) => updateCategory(cat));
ipcMain.handle('getInvoicesForRange', async (event, start, end) =>
  getInvoicesForRange(start, end),
);
ipcMain.handle('getSalesForRange', async (event, start, end) =>
  getSalesForRange(start, end),
);
ipcMain.handle('getAllProducts', async () => getAllProducts());
ipcMain.handle('getSalesByProduct', async (event, id) => getSalesByProduct(id));
ipcMain.handle('getCategoriesAndSubcategories', async () =>
  getCategoriesAndSubcategories(),
);
ipcMain.handle('deleteCategory', async (event, category) =>
  deleteCategory(category),
);
ipcMain.handle('getExcelReport', (event, start, end) =>
  getCSVforRange(start, end),
);

const installExtensions = async () => {
  const installer = require('electron-devtools-installer');
  const forceDownload = !!process.env.UPGRADE_EXTENSIONS;
  const extensions = ['REACT_DEVELOPER_TOOLS'];

  return installer
    .default(
      extensions.map((name) => installer[name]),
      forceDownload,
    )
    .catch(console.log);
};

const createWindow = async () => {
  if (isDebug) {
    await installExtensions();
  }

  const RESOURCES_PATH = app.isPackaged
    ? path.join(process.resourcesPath, 'assets')
    : path.join(__dirname, '../../assets');

  const getAssetPath = (...paths: string[]): string => {
    return path.join(RESOURCES_PATH, ...paths);
  };

  mainWindow = new BrowserWindow({
    show: false,
    width: 1024,
    height: 728,
    icon: getAssetPath('icon.png'),
    webPreferences: {
      devTools: true,
      preload: app.isPackaged
        ? path.join(__dirname, 'preload.js')
        : path.join(__dirname, '../../.erb/dll/preload.js'),
    },
  });

  mainWindow.loadURL(resolveHtmlPath('index.html'));

  mainWindow.on('ready-to-show', () => {
    if (!mainWindow) {
      throw new Error('"mainWindow" is not defined');
    }
    if (process.env.START_MINIMIZED) {
      mainWindow.minimize();
    } else {
      mainWindow.show();
    }
  });

  mainWindow.on('closed', () => {
    mainWindow = null;
  });

  const menuBuilder = new MenuBuilder(mainWindow);
  menuBuilder.buildMenu();

  // Open urls in the user's browser
  mainWindow.webContents.setWindowOpenHandler((edata) => {
    shell.openExternal(edata.url);
    return { action: 'deny' };
  });

  // Remove this if your app does not use auto updates
  // eslint-disable-next-line
  new AppUpdater();
};

/**
 * Add event listeners...
 */

app.on('window-all-closed', () => {
  // Respect the OSX convention of having the application in memory even
  // after all windows have been closed
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app
  .whenReady()
  .then(() => {
    createWindow();
    app.on('activate', () => {
      // On macOS it's common to re-create a window in the app when the
      // dock icon is clicked and there are no other windows open.
      if (mainWindow === null) createWindow();
    });
  })
  .catch(console.log);
