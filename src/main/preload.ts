// Disable no-unused-vars, broken for spread args
/* eslint no-unused-vars: off */
import { contextBridge, ipcRenderer, IpcRendererEvent } from 'electron';
import * as types from './types';

export type Channels = 'ipc-example';

const electronHandler = {
  ipcRenderer: {
    sendMessage(channel: Channels, ...args: unknown[]) {
      ipcRenderer.send(channel, ...args);
    },
    on(channel: Channels, func: (...args: unknown[]) => void) {
      const subscription = (_event: IpcRendererEvent, ...args: unknown[]) =>
        func(...args);
      ipcRenderer.on(channel, subscription);

      return () => {
        ipcRenderer.removeListener(channel, subscription);
      };
    },
    once(channel: Channels, func: (...args: unknown[]) => void) {
      ipcRenderer.once(channel, (_event, ...args) => func(...args));
    },
  },
  dbQuery: (query: string) => ipcRenderer.invoke('genericDBQuery', query),
  getCategories: () => ipcRenderer.invoke('getCategories'),
  getCategory: (id: number) => ipcRenderer.invoke('getCategorybyId', id),
  getSubcategories: (id: number) => ipcRenderer.invoke('getSubcategories', id),
  getProduct: (id: number) => ipcRenderer.invoke('getProductById', id),
  getCashiers: () => ipcRenderer.invoke('getCashiers'),
  saveCashier: (initials: string) =>
    ipcRenderer.invoke('saveCashier', initials),
  deleteCashier: (initials: number) =>
    ipcRenderer.invoke('deleteCashier', initials),
  getCashier: (id: number) => ipcRenderer.invoke('getCashierById', id),
  getInvoice: (num: string) => ipcRenderer.invoke('getInvoiceByNum', num),
  saveTransaction: (invoice: types.IncomingInvoice) =>
    ipcRenderer.invoke('saveTransaction', invoice),
  addCategory: (cat: types.Category) => ipcRenderer.invoke('addCategory', cat),
  addProduct: (prod: types.NewProduct) =>
    ipcRenderer.invoke('addProductToInventory', prod),
  deleteProduct: (id: number) =>
    ipcRenderer.invoke('deleteProductFromInventory', id),
  getProductsByCategory: (category: number) =>
    ipcRenderer.invoke('getProductsByCategory', category),
  updateProduct: (prod: types.PlainProduct) =>
    ipcRenderer.invoke('updateProduct', prod),
  updateProductInventory: (prod: types.InventoryCount) =>
    ipcRenderer.invoke('updateProductInventory', prod),
  decreaseInventory: (prod: number, dec: number) =>
    ipcRenderer.invoke('decreaseInventory', prod, dec),
  getInventoryByProduct: (id: number) =>
    ipcRenderer.invoke('getInventoryByProduct', id),
  updateCategory: (category: types.Category) =>
    ipcRenderer.invoke('updateCategory', category),
  getInvoicesForRange: (start: string, end: string) =>
    ipcRenderer.invoke('getInvoicesForRange', start, end),
  getSalesForRange: (start: string, end: string) =>
    ipcRenderer.invoke('getSalesForRange', start, end),
  getAllProducts: () => ipcRenderer.invoke('getAllProducts'),
  getSalesByProduct: (id: number) =>
    ipcRenderer.invoke('getSalesByProduct', id),
  getCategoriesAndSubcategories: () =>
    ipcRenderer.invoke('getCategoriesAndSubcategories'),
  deleteCategory: (category: types.Category) =>
    ipcRenderer.invoke('deleteCategory', category),
  getExcelReport: (start: string, end: string) =>
    ipcRenderer.invoke('getExcelReport', start, end),
};

contextBridge.exposeInMainWorld('electron', electronHandler);

export type ElectronHandler = typeof electronHandler;
