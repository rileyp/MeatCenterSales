export type Category = {
  categoryId: number;
  name: string;
  taxRate: number;
};

export type FullCategory = Category & {
  subcategories: string[];
};

export type Product = {
  productId: number;
  name: string;
  unitPrice: number;
  productCode: number;
  category: Category;
  subcategory: string;
  productKey: number;
};

export type PlainProduct = {
  productKey: number;
  productId: number;
  name: string;
  unitPrice: number;
  productCode: number;
  categoryId: number;
  subcategory: string;
};

export type DBProduct = PlainProduct;

export type DBProductCount = PlainProduct & {
  inventoryId: number;
  quantity: number;
};

export type NewProduct = Omit<PlainProduct, 'productKey'> & {
  initialInventory: number;
};

export type Inventory = {
  inventoryId: number;
  productKey: number;
  quantity: number;
};

export type Cashier = {
  initials: string;
  cashierId: number;
};

export type InvoiceProduct = {
  invoiceProductId: number;
  weight: number;
  subtotal: number;
  invoiceId: number;
  productKey: number;
  productId: number;
  category: string;
  description: string;
  quanitity: number;
  price: number;
};

export type IncomingInvoiceProduct = Omit<
  InvoiceProduct,
  'invoiceProductId' | 'invoiceId' | 'productKey'
>;

export type InventoryCount = {
  productId: number;
  quantity: number;
};

export type DBInventory = InventoryCount & {
  inventoryId: number;
};

export type BaseInvoice = {
  date: string;
  total: number;
  subtotal: number;
  clerkDailyOrder: number;
  invoiceNum: string;
  ourOrderNum: string;
  yourOrderNum: string;
  terms: string;
  salesRep: string;
  discount: number;
  soldTo: string;
  shippedTo: string;
  paymentMethod: string;
  salesRepEmail: string;
  clerkInitials: string;
};

export type Invoice = BaseInvoice & {
  invoiceId: number;
  orderItems: InvoiceProduct[];
};

export type IncomingInvoice = BaseInvoice & {
  orderItems: IncomingInvoiceProduct[];
};

export type DBInvoice = BaseInvoice & {
  invoiceId: number;
};

export type Sale = {
  date: string;
  total: number;
};

export type SalesRange = {
  check: Sale[];
  card: Sale[];
};

export type ProductSale = {
  weight: number;
  date: string;
  subtotal: number;
};

export type SubcategoryProducts = {
  name: string;
  products: DBProductCount[];
};

export type CategoryProducts = {
  category: Category;
  subcategories: SubcategoryProducts[];
};
